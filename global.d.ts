declare module "*.png";
declare module "*.svg";
declare module "*.jpeg";
declare module "*.jpg";

export interface ImportMetaEnv {
  readonly VITE_API_URL: string
}

export interface ImportMeta {
  readonly env: ImportMetaEnv
}