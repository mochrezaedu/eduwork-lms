import { ActionReducerMapBuilder, createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios"
import { ClassServiceInterface } from "./class-type"

export const fetchAllClass = createAsyncThunk('services/class/fetchAllClass', async(_,{rejectWithValue}) => {
  try {
    const resp = await axios.get(`class/all`)
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})
export const fetchAllClassHandler = (builder: ActionReducerMapBuilder<ClassServiceInterface>) => {
  builder
    .addCase(fetchAllClass.pending, (state) => {
      // state.isLoading = true;
      // state.error = null;
    })
    .addCase(fetchAllClass.fulfilled, (state, action) => {
      // state.isLoading = false;
      const newData = {...state.data, data: [...action.payload]}
      state.data = newData
    })
    .addCase(fetchAllClass.rejected, (state, action) => {
      // state.isLoading = false;
      // state.error = action.payload;
    });
};