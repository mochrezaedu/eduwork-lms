import { ActionReducerMapBuilder, PayloadAction, createSlice } from "@reduxjs/toolkit"
import { ClassServiceInterface } from "./class-type"
import { isClass } from "../../types/model"
import { RootState } from "../../types/redux"
import { fetchAllClassHandler } from "./api"

const initialState: ClassServiceInterface = {
  data: {
    data: []
  },
  loads: []
}

export const classService = createSlice({
  name: 'class',
  initialState,
  reducers: {
    allClass: (state, action: PayloadAction<isClass>) => {
      console.log(state)
      // state.list = action.payload
    },
  }, 
  extraReducers: (builder: ActionReducerMapBuilder<ClassServiceInterface>) => {
    fetchAllClassHandler(builder)
  }
})

// Action creators are generated for each case reducer function
export const classActions = classService.actions

export const allClasses = (state:RootState) => state.classService.data.data
export const getClassLoads = (state:RootState) => state.classService.loads

export default classService