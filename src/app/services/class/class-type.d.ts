
export interface ClassServiceInterface {
  data: {
    data: Array<Record<string,any>>;
    [key: string]: any;
  };
  loads: Array<string>;
}