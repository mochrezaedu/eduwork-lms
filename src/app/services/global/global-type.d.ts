
export type GlobalServiceType = {
  theme: string;
  sidebarShow: "icon" | "full";
}