import { ActionReducerMapBuilder, createSlice, Draft, PayloadAction } from "@reduxjs/toolkit"
import { GlobalServiceType } from "./global-type"
import { RootState } from "../../types/redux"

const initialState: GlobalServiceType = {
  theme: '',
  sidebarShow: "full"
}

const reducers = {
  toggleSidebarShow: (state: Draft<GlobalServiceType>) => {
    state.sidebarShow = (state.sidebarShow === "full" ? "icon": "full")
  },
  setSidebarShow: (state: Draft<GlobalServiceType>, action?: PayloadAction<"full" | "icon">) => {
    state.sidebarShow = action?.payload ?? (state.sidebarShow === "full" ? "icon": "full")
  }
}

const globalService = createSlice({
  name: 'globalService',
  initialState,
  reducers,
  extraReducers: (builder: ActionReducerMapBuilder<GlobalServiceType>) => {
    // extraReducers(builder)
  }
})

export const globalActions = globalService.actions

export const globalStates = (state: RootState) => state.globalService

export default globalService