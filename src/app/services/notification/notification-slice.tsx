import { RootState } from "@/src/app/types/redux";
import { ActionReducerMapBuilder, PayloadAction, createSelector, createSlice } from "@reduxjs/toolkit";
import { NotificationPageInterface } from "./notification-type";
import { fetchAllNotificationHandler, fetchTodoEventHandler } from "./api";

export const initialState: NotificationPageInterface = {
  "current_page": 0,
  "data": [],
  "first_page_url": null,
  "from": 0,
  "last_page": null,
  "last_page_url": null,
  "links": [],
  "next_page_url": null,
  "path": null,
  "per_page": 5,
  "prev_page_url": null,
  "to": 0,
  "total": 0,
  "loads": ['fetch_notification']
}

const notificationService = createSlice({
  name: 'page/notification',
  initialState,
  reducers: {
    removeLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.filter((item) => item != action.payload)
      state.loads = newLoads
    },
    addLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.find((item) => item == action.payload)
      if (!newLoads) {
        state.loads = [...state.loads, action.payload]        
      }
      return state
    },
    readNotification: (state, action) => {
      const newData = state.data.filter(item => item.id !== action.payload);
      state.data = newData
    },
    readableNotification: (state, action) => {
      const existingDataIndex = state.data.findIndex((item) => item.id === action.payload)
      if (existingDataIndex > -1) {
        const newData = [...state.data]  // Create a new array to avoid mutating the original state
        newData[existingDataIndex] = { ...newData[existingDataIndex], can_read: true }
        return { ...state, data: newData } // Return the updated state
      }
      return state; // Return the original state if no update was made
    },    
    unreadableNotification: (state, action) => {
      const existingDataIndex = state.data.findIndex((item) => item.id === action.payload)
      if (existingDataIndex > -1) {
        const newData = state.data
        newData[existingDataIndex] = {...newData[existingDataIndex], can_read: false}
        state.data = [...newData]
      }
    },
    cleanupNotification: (state) => {
      return {...initialState}
    }
  },
  extraReducers: (builder: ActionReducerMapBuilder<NotificationPageInterface>) => {
    fetchAllNotificationHandler(builder)
    fetchTodoEventHandler(builder)
  }
})

export const {
  removeLoad,
  addLoad,
  cleanupNotification
} = notificationService.actions

export const notificationActions = notificationService.actions

export const getLoads = (state: RootState) => state.notificationService.loads
export const notificationAll = (state: RootState) => state.notificationService.data.filter(item => (item.status == 'unread'))
export const notificationStates = (state: RootState) => state.notificationService
export const notificationEventProgress = createSelector([notificationAll, (state:RootState,id) => {
  return id
}], (item, id) => {
  return item.sort((a,b) => {
    var keyA = new Date(a.created_at),
      keyB = new Date(b.created_at);
    // Compare the 2 dates
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  })
})

export default notificationService