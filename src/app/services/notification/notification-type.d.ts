
export interface NotificationPageInterface {
  "loads": Array<string>,
  "current_page": number,
  "data": Array<isNotification>,
  "first_page_url": number | null,
  "from": number,
  "last_page": number | null,
  "last_page_url": string | number | null,
  "links": [],
  "next_page_url": string | number | null,
  "path": string | number | null,
  "per_page": number,
  "prev_page_url": string | number | null,
  "to": string | number,
  "total": string | number
}

export interface isNotification {
  "id": number;
  "user_id": number;
  "target_id": number | null;
  "title": null | string;
  "description": null | string;
  "url_redirect": null | string;
  "status": null | string;
  "type": null | string;
  "class_id": null | string;
  "created_at": string;
  "updated_at": string;
  "priority_text": null | string;
  "priority": number;
  "can_read"?: Boolean;
  "type_prefix": string | null;
}