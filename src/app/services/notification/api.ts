import { ActionReducerMapBuilder, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { addLoad, removeLoad } from "@/src/app/services/notification/notification-slice";
import { NotificationPageInterface, isNotification } from "./notification-type";
import { startEvent } from "../event/api";

export const fetchAllNotification = createAsyncThunk('services/notification/fetchAllNotification', async(_: any, {dispatch,rejectWithValue}) => {
  try {
    const resp = await axios.get(`/notification/all`, {
      params: _.payload
    })
    const data = await resp.data
    dispatch(removeLoad('fetch_notification'))
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})
export const fetchAllNotificationHandler = (builder: ActionReducerMapBuilder<NotificationPageInterface>) => {
  builder
    .addCase(fetchAllNotification.fulfilled, (state, action) => {
      const {payload, ...others} = action
      const newIds: Array<number> = payload.data.map((item:isNotification) => item.id)
      const removedData = state.data.filter(item => !newIds.includes(item.id))
      const newData = [...removedData, ...payload.data]
      const newState = {...state, ...others, data: newData}
      return newState
    })
    .addCase(fetchAllNotification.rejected, (state, action) => {
      alert('Error fetch Notification!')
    });
};


export const fetchTodoEvent = createAsyncThunk('services/notification/fetchTodoEvent', async(_: any, {dispatch,rejectWithValue}) => {
  dispatch(addLoad("fetch_todo"))
  try {
    const resp = await axios.get(`notification/event_todo`, {
      params: _.payload
    })
    const data = await resp.data
    if (_.type == 'reload') {
      await dispatch(startEvent({
        payload: {..._.payload}
      }))
      await dispatch(fetchTodoEvent({
        payload: {..._.payload}
      }))
    }
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})

export const fetchTodoEventHandler = (builder: ActionReducerMapBuilder<NotificationPageInterface>) => {
  builder.addCase(fetchTodoEvent.fulfilled, (state, action) => {
    const {payload, ...others} = action
    const newIds: Array<number> = payload.data.map((item:isNotification) => item.id)
    const removedData = state.data.filter(item => !newIds.includes(item.id))
    const newData = [...removedData, ...payload.data]
    const newLoads = state.loads.filter((item) => item != "fetch_todo")


    const newState = {...state, ...others, data: newData, loads: newLoads} 
    return newState
  })
}

export const readNotification = createAsyncThunk('services/notification/read_notification', async(_: Record<any, any>, {rejectWithValue}) => {
  try {
    const resp = await axios.post('notification/read_notification', _.payload)
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue(error || 'An error occurred')
  }
})