import { defaultApiClient } from "@/src/utils/api-client";
import { ActionReducerMapBuilder, createAsyncThunk } from "@reduxjs/toolkit";
import { AuthServiceInterface } from "../../types/services/auth";
import { AxiosWrapper } from '@/src/utils/axios'
import { config } from "../../config";
import { fetchEventTypes } from "../event/api";
import { removeLoad } from "@/src/pages/init/init-slice";
import { fetchAllClass } from "../class/api";

export const getAuthUser = defaultApiClient('auth/auth_auth', config.APP_URL+'/ap/lmsmentorapi/get_auth', {
  method: 'POST',
  credentials: "include",
  body: JSON.stringify({})
})

export const fetchUserHandler = (builder: ActionReducerMapBuilder<AuthServiceInterface>) => {
  builder.addCase(getAuthUser.pending,(state, action) => {
    
  }).addCase(getAuthUser.fulfilled,(state, action) => {
    const newState = {...state, user: {...action.payload}}
    const token = action.payload?.api_token ?? ''
    localStorage.setItem('api_token', token)
    AxiosWrapper.init({
      authorization: token
    }) 
    return newState 
  }).addCase(getAuthUser.rejected,(state, action) => {
    
  })
}

export const setAuthentication = createAsyncThunk('auth/authenticating', async (_, {dispatch}) => {
  await new Promise((resolve) => {
    (setTimeout(resolve, Math.floor(Math.random() * 1000) + 500))
  }).then(() => {
    dispatch(fetchEventTypes())
    dispatch(removeLoad('fetch_event_types'))
  })

  await new Promise((resolve) => {
    (setTimeout(resolve, Math.floor(Math.random() * 1000) + 100))
  }).then(() => {
    dispatch(fetchAllClass())
    dispatch(removeLoad('fetch_classes'))
  })
})