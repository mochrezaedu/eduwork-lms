import { ActionReducerMapBuilder, createSlice } from "@reduxjs/toolkit";
import { AuthServiceInterface } from "../../types/services/auth";
import { fetchUserHandler } from "./auth-api";
import { RootState } from "../../types/redux";

const initialState: AuthServiceInterface = {
  user: null
}

const authSlice = createSlice({
  name: 'service.auth',
  initialState,
  reducers: {},
  extraReducers: (builder: ActionReducerMapBuilder<AuthServiceInterface>) => {
    fetchUserHandler(builder)
  }
})

export const getAuth = (state: RootState) => {
  return state.auth
}

export default authSlice
