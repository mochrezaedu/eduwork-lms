import { ActionReducerMapBuilder, Draft, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { isRegistrant } from "./registrant-type";
import { EventServiceInterface } from "../event/event-type";
import eventTodoService, { eventTodoActions } from "../event/event-todo-slice";

export const registrantsForMentorReviews = createAsyncThunk("service/registrant/for_mentor_reviews", async(_: any, {rejectWithValue,dispatch}) => {
  dispatch(eventTodoActions.addLoad("fetch_registrants_for_mentor_reviews"))
  try {
    const resp = await axios.get(`registrant/registrant_event_reviews`, {
      params: _.payload
    })
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})

export const registrantsForMentorReviewsHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(registrantsForMentorReviews.fulfilled, (state: Draft<EventServiceInterface>, action: PayloadAction<isRegistrant>) => {
    eventTodoService.caseReducers.addRegistrantForReviewMentor(state,action)
  })
}