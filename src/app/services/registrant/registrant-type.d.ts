
export interface RegistrantPageInterface {
  "loads": Array<string>,
  "current_page": number,
  "data": Array<isRegistrant>,
  "first_page_url": number | null,
  "from": number,
  "last_page": number | null,
  "last_page_url": string | number | null,
  "links": [],
  "next_page_url": string | number | null,
  "path": string | number | null,
  "per_page": number,
  "prev_page_url": string | number | null,
  "to": string | number,
  "total": string | number
}

interface RegistrantRelationType {
  student?: Record<any,any>
}

export interface isRegistrant extends RegistrantRelationType {
  note_labels?: Record<any,any>;
  id: number;
  class_id: number | null;
  student_id: number | null;
  note: string | null;
  mentor_note: string | null;
  monitor_student_note: string | null;
  code: string | null;
  registrant_reference: string | null;
  created_at: string | null;
  updated_at: string | null;
  status: string | null;
  update_status_date: string | null;
  time_move_approved: string | null;
  sikon: string | null;
  jaminan_mou: string | null;
  jaminan_ijazah: string | null;
  jaminan_uang: string | null;
  jaminan_other: string | null;
  jaminan_kontak: string | null;
  commitment: number | null;
  interview: string | null;
  confirm_date: string | null;
  status_doc: string | null;
  status_transfer: string | null;
  commitment_fee_terbayar: number | null;
  bukti_pembayaran: string | null;
  reject_reason: string | null;
  document_resi: string | null;
  rating: number | null;
  status_ijazah: string | null;
  document_diterima: string | null;
  document_dikirim: string | null;
  skill: string | null;
  berkas_save: string | null;
  contract: string | null;
  contract_review: string | null;
  type_class: string | null;
  deleted_at: string | null;
  sort: number | null;
  finish_class: number | null;
  payment_method: string | null;
  tenor: string | null;
  payment_plus: number | null;
  payment_minus: number | null;
  payment_note: string | null;
  total_price: number | null;
  commitment_fee: number | null;
  credit_status: string | null;
  test_question_count: number | null;
  progress: number | null;
  mentor_progress: number | null;
  mentor_curriculum_position: string | null;
  after_submit_ads: number | null;
  status_sertifikat: string | null;
  after_lead_ads: number | null;
  freeup_at: string | null;
  show: string | null;
  credit_not_paid: number | null;
  start_class_at: string | null;
  additional_payment_method: string | null;
  latest_student_progress_date: string | null;
  class_packet_id: number | null;
  last_follow_up: string | null;
  class_progress: number | null;
  rmentor_progress: number | null;
  r_progress: number | null;
  rclass_progress: number | null;
  alert_progress_late_message: string | null;
  voucher_code: string | null;
  voucher_id: number | null;
  target_next_curriculum: string | null;
  target_late_curriculum: string | null;
  time_move_to_paid: string | null;
}
