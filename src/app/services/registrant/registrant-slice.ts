import { createSlice } from "@reduxjs/toolkit";
import { RegistrantPageInterface } from "./registrant-type";
import BaseReducer from "@/src/utils/base-reducers";
import { RootState } from "../../types/redux";

const initialState: RegistrantPageInterface = {
  "current_page": 0,
  "data": [],
  "first_page_url": null,
  "from": 0,
  "last_page": null,
  "last_page_url": null,
  "links": [],
  "next_page_url": null,
  "path": null,
  "per_page": 10,
  "prev_page_url": null,
  "to": 0,
  "total": 0,
  "loads": [],
}

const registrantService = createSlice({
  name: "service.registrant",
  initialState,
  reducers: {
    ...BaseReducer
  },
  
})

export const registrantActions = registrantService.actions

export const registrantAll = (state: RootState) => state.registrantService.data
export const registrantStates = (state: RootState) => state.registrantService
export const registrantLoads = (state: RootState) => state.registrantService.loads

export default registrantService