import { ActionReducerMapBuilder, PayloadAction, createSelector, createSlice } from "@reduxjs/toolkit";
import { EventServiceInterface } from "./event-type";
import { destroyEventHandler, fetchAllEventHandler, fetchEventTypesHandler, fetchNavbarEventHandler, startEventHandler, storeEventHandler, updateEventHandler } from "./api";
import { RootState } from "../../types/redux";

export const initialState: EventServiceInterface = {
  "data": [],
  "per_page": 10,
  "first_page_url": null,
  "from": 0,
  "last_page": null,
  "last_page_url": null,
  "links": [],
  "next_page_url": null,
  "path": null,
  "prev_page_url": null,
  "to": 0,
  "total": 0,
  "members": [],
  "navbar": {
    "data": [],
    "per_page": 3,
    "active": null,
    "first_page_url": null,
    "from": 0,
    "last_page": null,
    "last_page_url": null,
    "links": [],
    "next_page_url": null,
    "path": null,
    "prev_page_url": null,
    "to": 0,
    "total": 0
  },
  "types": [],
  "loads": [],
  "messages": []
}

const eventService = createSlice({
  name: 'eventService',
  initialState,
  reducers: {
    removeLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.filter((item) => item != action.payload)
      state.loads = newLoads
      return state
    },
    addLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.find((item) => item == action.payload)
      if (!newLoads) {
        state.loads = [...state.loads, action.payload]        
      }
      return state
    },
    cleanupEvent: (state) => {
      return {...initialState, ...{navbar: state.navbar}}
    }
  },
  extraReducers: (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
    fetchNavbarEventHandler(builder)
    fetchAllEventHandler(builder)
    startEventHandler(builder)
    fetchEventTypesHandler(builder)
    storeEventHandler(builder)
    updateEventHandler(builder)
    destroyEventHandler(builder)
  }
})

export const eventActions = eventService.actions

export const stickyNavbarEvent = (state: RootState) => state.eventService.navbar
export const stickyNavbarEventData = (state: RootState) => state.eventService?.navbar?.data
export const eventStates = (state: RootState) => state.eventService
export const eventAll = (state: RootState) => state.eventService.data
export const getEventLoads = (state: RootState) => state.eventService.loads
export const eventTypes = (state: RootState) => state.eventService.types
export const eventById = createSelector([eventAll, (state:RootState, id) => id], (events, id) => {
  const resp = events.find((item) => item.id == id)
  return resp
})

export default eventService