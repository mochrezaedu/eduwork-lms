import { PayloadAction, createSelector, createSlice } from "@reduxjs/toolkit";
import { EventServiceInterface } from "./event-type";
import { RootState } from "../../types/redux";
import { Collection } from "@/src/utils/collection";
import { registrantsForMentorReviewsHandler } from "../registrant/api";

export const initialState: EventServiceInterface = {
  "data": [],
  "mentor_review_registrants": [],
  "reviewed_registrants": [],
  "read_ids": [],
  "per_page": 10,
  "first_page_url": null,
  "from": 0,
  "last_page": null,
  "last_page_url": null,
  "links": [],
  "next_page_url": null,
  "path": null,
  "prev_page_url": null,
  "to": 0,
  "total": 0,
  "loads": [],
  "messages": []
}

const eventTodoService = createSlice({
  name: 'eventTodoService',
  initialState,
  reducers: {
    removeLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.filter((item) => item != action.payload)
      state.loads = newLoads
      return state
    },
    addLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.find((item) => item == action.payload)
      if (!newLoads) {
        state.loads = [...state.loads, action.payload]        
      }
      return state
    },
    addRegistrantForReviewMentor: (state, action) => {
      eventTodoService.caseReducers.removeLoad(state,{
        ...action,
        payload: "fetch_registrants_for_mentor_reviews"
      })
      state.mentor_review_registrants = [...action.payload]
    },
    cleanupEvent: (state) => {
      return {...initialState, ...{navbar: state.navbar}}
    },
    addReviewedRegistrantIds: (state, action) => {
      state.reviewed_registrants = state.reviewed_registrants?.concat([action.payload])
    },
    removeReviewedRegistrantIds: (state, action) => {
      state.reviewed_registrants = state.reviewed_registrants?.filter(item => item != action.payload)
    },
    readNotificationIds: (state, action) => {
      state.read_ids = state.read_ids?.concat([action.payload]) ?? []
    },
    storeMentorFeedback: (state, action) => {
      const existingDataIndex = state.mentor_review_registrants?.findIndex((item) => item.id == action.payload) ?? -1
      if (existingDataIndex > -1) {
        const newData = state.mentor_review_registrants ?? []
        newData.splice(existingDataIndex, 1, {...newData[existingDataIndex], feedback_from_mentor: {}})
        state.mentor_review_registrants = [...newData]
      }
    }
  },
  extraReducers: (builder) => {
    registrantsForMentorReviewsHandler(builder)
  }
})

export const eventTodoActions = eventTodoService.actions

export const eventTodoStates = (state: RootState) => state.eventTodoService
export const eventTodoAll = (state: RootState) => state.eventTodoService.data
export const getEventTodoLoads = (state: RootState) => state.eventTodoService.loads
export const eventById = createSelector([eventTodoAll, (state:RootState, id) => id], (events, id) => {
  const resp = new Collection(events).where("id", id).first()
  return resp
})
export const eventTodoReviewedRegistrants = (state:RootState) => state.eventTodoService.reviewed_registrants
export const eventTodoReviewedRegistrantById = createSelector([eventTodoReviewedRegistrants, (state:RootState, id) => id], (reviewedRegistrants, id) => {
  return reviewedRegistrants?.find(item => item === id)
})


export const registrantEventMentorReviews = (state:RootState) => state.eventTodoService.mentor_review_registrants
export const registrantEventMentorReviewsNotRead = (state:RootState) => {
  return state.eventTodoService.mentor_review_registrants?.filter(item => !state.eventTodoService.read_ids?.includes(item.id)) ?? []
}


export default eventTodoService