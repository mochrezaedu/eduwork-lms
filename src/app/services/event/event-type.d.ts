
export interface EventServiceInterface {
  "data": Array<isEvent>,
  "first_page_url": number | null,
  "from": number,
  "last_page": number | null,
  "last_page_url": string | number | null,
  "links": [],
  "next_page_url": string | number | null,
  "path": string | number | null,
  "per_page": number,
  "prev_page_url": string | number | null,
  "to": string | number,
  "total": string | number,
  "mentor_review_registrants"?: Array<isRegistrant>,
  "reviewed_registrants"?: Array<number>,
  "read_ids"?: Array<number>,
  "members"?: Array<Record<any,any>>,
  "navbar"?: {
    "active": number | null,
    "data": Array<isEvent>,
    "first_page_url": number | null,
    "from": number,
    "last_page": number | null,
    "last_page_url": string | number | null,
    "links": [],
    "next_page_url": string | number | null,
    "path": string | number | null,
    "per_page": number,
    "prev_page_url": string | number | null,
    "to": string | number,
    "total": string | number
  };
  "types"?: Array<Record<any,any>>,
  "loads": Array<string>,
  "messages": Array<string>
}

export interface isEvent {
  id: number,
  title: string,
  slug: string,
  date: string | null,
  description: string | null,
  type: string | null,
  absentable: string;
  user_id: number | null;
  class_id: number | null;
  created_at: string;
  updated_at: string;
  link_youtube: string | null;
  deleted_at: string | null;
  schedule_id: number | null;
  link_record: number | null;
  e_date: string | null;
  status: string | null;
}