import { ActionReducerMapBuilder, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { EventServiceInterface } from "./event-type";
import eventService, { eventActions } from "./event-slice";

export const startEvent = createAsyncThunk('services/event/startEvent', async(_: any, {dispatch,rejectWithValue}) => {
  try {
    const resp = await axios.post(`event/start`, {
      ..._.payload
    })
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})

export const startEventHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder
  .addCase(startEvent.pending, (state, action) => {
    eventService.caseReducers.addLoad(state, {
      ...action,
      payload: "start_event"
    })
  })
  .addCase(startEvent.fulfilled, (state, action) => {
    eventService.caseReducers.removeLoad(state, {
      ...action,
      payload: "start_event"
    })
  })
}

export const fetchAllEvent = createAsyncThunk('services/event/fetchAllEvent', async(_: any, {dispatch,rejectWithValue}) => {
  dispatch(eventActions.addLoad("fetch_all"))
  try {
    const resp = await axios.get(`event/all`, {
      params: _.payload
    })
    const data = await resp.data
    dispatch(eventActions.removeLoad("fetch_all"))
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})

export const fetchAllEventHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(fetchAllEvent.fulfilled, (state, {payload}) => {
    const newState = {...state, ...payload}
    return newState
  })
}

export const fetchNavbarEvent = createAsyncThunk('services/event/fetchNavbarEvent', async(_: any, {dispatch,rejectWithValue}) => {
  dispatch(eventActions.addLoad("fetch_navbar"))
  try {
    const resp = await axios.get(`event/all_progress`, {
      params: _.payload
    })
    const data = await resp.data
    dispatch(eventActions.removeLoad("fetch_navbar"))
    return data
  } catch (error) {
    return rejectWithValue('An error occurred')
  }
})

export const fetchNavbarEventHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(fetchNavbarEvent.fulfilled, (state, {payload}) => {
    const newNavbar = {
      navbar: {...state.navbar, ...payload}
    }
    const newState = {...state, ...newNavbar}
    return newState
  })
}

export const storeFeedbackMentor = createAsyncThunk('services/event/store_todo_review', async(_: Record<any, any>, {rejectWithValue}) => {
  try {
    const resp = await axios.post('event/store_todo_review', _.payload)
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue(error || 'An error occurred')
  }
})

export const fetchEventTypes = createAsyncThunk('services/event/get_event_types', async(_, {rejectWithValue, dispatch}) => {
  dispatch(eventActions.addLoad("fetch_types"))
  try {
    const resp = await axios.get('event/get_event_types')
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue(error || 'An error occurred')
  }
})
export const fetchEventTypesHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(fetchEventTypes.fulfilled, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "fetch_types"
    })
    state.types = [...action.payload];
  })
}

export const storeEvent = createAsyncThunk('services/event/stores', async(_: Record<any, any>, {rejectWithValue}) => {
  try {
    const resp = await axios.post('event/store', _.payload)
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue(error || 'An error occurred')
  }
})

export const storeEventHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(storeEvent.pending, (state, action) => {
    eventService.caseReducers.addLoad(state,{
      ...action,
      payload: "store_event"
    });
  }).addCase(storeEvent.fulfilled, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "store_event"
    });
    state.data.splice(0, 0, action.payload.data)
  }).addCase(storeEvent.rejected, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "store_event"
    });
  })
}

export const updateEvent = createAsyncThunk('services/event/update', async(_: Record<any, any>, {rejectWithValue}) => {
  try {
    const resp = await axios.post('event/update', _.payload)
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue(error || 'An error occurred')
  }
})

export const updateEventHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(updateEvent.pending, (state, action) => {
    eventService.caseReducers.addLoad(state,{
      ...action,
      payload: "update_event"
    });
  }).addCase(updateEvent.fulfilled, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "update_event"
    });
    const index = state.data.findIndex(item => item.id === action.payload.data.id)
    if (index > -1) {
      state.data.splice(index, 1, action.payload.data)      
    }
  }).addCase(updateEvent.rejected, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "update_event"
    });
  })
}

export const destroyEvent = createAsyncThunk('services/event/destroy', async(_: Record<any, any>, {rejectWithValue}) => {
  try {
    const resp = await axios.post('event/destroy', _.payload)
    const data = await resp.data
    return data
  } catch (error) {
    return rejectWithValue(error || 'An error occurred')
  }
})

export const destroyEventHandler = (builder: ActionReducerMapBuilder<EventServiceInterface>) => {
  builder.addCase(destroyEvent.pending, (state, action) => {
    eventService.caseReducers.addLoad(state,{
      ...action,
      payload: "destroy_event"
    });
  }).addCase(destroyEvent.fulfilled, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "destroy_event"
    });
    const index = state.data.findIndex(item => item.id == action.payload.data.id);
    if (index > -1) {
      state.data.splice(index, 1)      
    }
  }).addCase(destroyEvent.rejected, (state, action) => {
    eventService.caseReducers.removeLoad(state,{
      ...action,
      payload: "destroy_event"
    });
  })
}
