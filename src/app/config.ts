// @ts-nocheck

export const config = {
  APP_URL: import.meta.env.VITE_APP_URL as string,
  API_URL: import.meta.env.VITE_API_URL as string,
  SESSION_NAME: import.meta.env.VITE_SESSION_NAME
}