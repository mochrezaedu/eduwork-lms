import {
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit'

type Registrant = { id: number; title: string }

const registrantsAdapter = createEntityAdapter<Registrant>({
  // Assume IDs are stored in a field other than `book.id`
  selectId: (registrant) => registrant.id,
  // Keep the "all IDs" array sorted based on book titles
  sortComparer: (a, b) => a.title.localeCompare(b.title),
})

const registrantsSlice = createSlice({
  name: 'books',
  initialState: registrantsAdapter.getInitialState(),
  reducers: {
    // Can pass adapter functions directly as case reducers.  Because we're passing this
    // as a value, `createSlice` will auto-generate the `bookAdded` action type / creator
    bookAdded: registrantsAdapter.addOne,
    registrantsReceived(state, action) {
      // Or, call them as "mutating" helpers in a case reducer
      registrantsAdapter.setAll(state, action.payload.registrants)
    },
  },
})


// { ids: [], entities: {} }

// Can create a set of memoized selectors based on the location of this entity state
// const booksSelectors = booksAdapter.getSelectors<RootState>(
//   (state) => state.books
// )

// And then use the selectors to retrieve values
// export const allRegistrant = booksSelectors.selectAll(store.getState())

export default registrantsSlice