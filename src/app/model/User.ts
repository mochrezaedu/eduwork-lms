export interface isUser {
  id: number,
  name: string,
  email: string,
  phone: string,
  birthday: string,
  api_token?: string | null
}