import { configureStore } from '@reduxjs/toolkit'
import { classSlice } from '../pages/class/slice-class'
import modalSlice from '../components/modal/slice-modal';
import authSlice from './services/auth/auth-slice';
import initSlice from '../pages/init/init-slice';
import notificationService from './services/notification/notification-slice';
import eventService from './services/event/event-slice';
import eventTodoService from './services/event/event-todo-slice';
import registrantService from './services/registrant/registrant-slice';
import classService from './services/class/class-slice';
import globalService from './services/global/global-slice';

const store = configureStore({
  reducer: {
    // PAGES
    classPage: classSlice.reducer,
    initPage: initSlice.reducer,
    globalService: globalService.reducer,

    // SERVICES    
    auth: authSlice.reducer,
    notificationService: notificationService.reducer,
    eventService: eventService.reducer,
    eventTodoService: eventTodoService.reducer,
    registrantService: registrantService.reducer,
    classService: classService.reducer,

    // COMPONENTS
    modal: modalSlice.reducer
  }
})

export default store