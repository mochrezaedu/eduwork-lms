export type isStudent = {
  id: string | number,
  name: string, 
  mentor_progress?: string | number, 
  progress: string | number, 
  mentor_mandatory?: string,
  notes?: Array<isNoteLabel>,
  phone?: string
}

export type isClass = {
  id: string | number,
  title: string,
  updated_at_humans?: string,registrants_count?: number,events_count?: number
}

export type isNoteLabel = {
  id: number | string,
  color: string | null | undefined,
  created_at: string,
  updated_at: string,
  user_id: string | number | null,
  value: string | null,
  label: isLabel | null
}

export type isLabel = {
  id: number | string,
  value: string | null,
  color: string | null
}