import { isClass } from "../model"

export interface PageClassState {
  list: isClass[],
  loading: any[],
  error: any[]
}