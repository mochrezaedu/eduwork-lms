import { FC } from "react";

export interface MenuType {
  label: string,
  url: string,
  type?: string,
  form?: {
    action: string,
    method: string
  },
  icon: Function
}

export type MenuInterface = Array<MenuType> 