import { isUser } from "../../model/User";

export interface AuthServiceInterface {
  user: isUser | null
}