import logoPng from '@/assets/logo.png'
import { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { MenuInterface, MenuType } from '../app/types/pages/sidebar'
import { config as AppConfig, config } from '../app/config'
import { useDispatch, useSelector } from 'react-redux'
import { globalActions, globalStates } from '../app/services/global/global-slice'
import { AnimatePresence,motion } from 'framer-motion'
import { AppDispatch } from '../app/types/redux'

const menus: MenuInterface = [
  {
    label: 'Notification',
    url: '/notification',
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
    <path strokeLinecap="round" strokeLinejoin="round" d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z" />
  </svg>
  },
  {
    label: 'My Class',
    url: '/class',
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
      <path strokeLinecap="round" strokeLinejoin="round" d="M12 6.042A8.967 8.967 0 006 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 016 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 016-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0018 18a8.967 8.967 0 00-6 2.292m0-14.25v14.25" />
    </svg>  
  },
  {
    label: 'Student',
    url: '/student',
    icon: () => 
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
        <path strokeLinecap="round" strokeLinejoin="round" d="M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5" />
    </svg>
  },
  {
    label: 'Event',
    url: '/event',
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-5 h-5">
      <path strokeLinecap="round" strokeLinejoin="round" d="M12 21a9.004 9.004 0 008.716-6.747M12 21a9.004 9.004 0 01-8.716-6.747M12 21c2.485 0 4.5-4.03 4.5-9S14.485 3 12 3m0 18c-2.485 0-4.5-4.03-4.5-9S9.515 3 12 3m0 0a8.997 8.997 0 017.843 4.582M12 3a8.997 8.997 0 00-7.843 4.582m15.686 0A11.953 11.953 0 0112 10.5c-2.998 0-5.74-1.1-7.843-2.918m15.686 0A8.959 8.959 0 0121 12c0 .778-.099 1.533-.284 2.253m0 0A17.919 17.919 0 0112 16.5c-3.162 0-6.133-.815-8.716-2.247m0 0A9.015 9.015 0 013 12c0-1.605.42-3.113 1.157-4.418" />
    </svg>
  },
  {
    label: 'Forum',
    url: `${config.APP_URL}/community`,
    type: 'redirect',
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
      <path strokeLinecap="round" strokeLinejoin="round" d="M18 18.72a9.094 9.094 0 003.741-.479 3 3 0 00-4.682-2.72m.94 3.198l.001.031c0 .225-.012.447-.037.666A11.944 11.944 0 0112 21c-2.17 0-4.207-.576-5.963-1.584A6.062 6.062 0 016 18.719m12 0a5.971 5.971 0 00-.941-3.197m0 0A5.995 5.995 0 0012 12.75a5.995 5.995 0 00-5.058 2.772m0 0a3 3 0 00-4.681 2.72 8.986 8.986 0 003.74.477m.94-3.197a5.971 5.971 0 00-.94 3.197M15 6.75a3 3 0 11-6 0 3 3 0 016 0zm6 3a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0zm-13.5 0a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0z" />
    </svg>
  },
  {
    label: 'Support',
    url: `${config.APP_URL}/support-ticket`,
    type: 'redirect',
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-[1.2rem] h-[1.2rem]">
      <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 13.5l10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75z" />
    </svg>  
  },
  {
    label: 'Setting',
    url: `${config.APP_URL}/profile/edit`,
    type: 'redirect',
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
      <path strokeLinecap="round" strokeLinejoin="round" d="M9.594 3.94c.09-.542.56-.94 1.11-.94h2.593c.55 0 1.02.398 1.11.94l.213 1.281c.063.374.313.686.645.87.074.04.147.083.22.127.324.196.72.257 1.075.124l1.217-.456a1.125 1.125 0 011.37.49l1.296 2.247a1.125 1.125 0 01-.26 1.431l-1.003.827c-.293.24-.438.613-.431.992a6.759 6.759 0 010 .255c-.007.378.138.75.43.99l1.005.828c.424.35.534.954.26 1.43l-1.298 2.247a1.125 1.125 0 01-1.369.491l-1.217-.456c-.355-.133-.75-.072-1.076.124a6.57 6.57 0 01-.22.128c-.331.183-.581.495-.644.869l-.213 1.28c-.09.543-.56.941-1.11.941h-2.594c-.55 0-1.02-.398-1.11-.94l-.213-1.281c-.062-.374-.312-.686-.644-.87a6.52 6.52 0 01-.22-.127c-.325-.196-.72-.257-1.076-.124l-1.217.456a1.125 1.125 0 01-1.369-.49l-1.297-2.247a1.125 1.125 0 01.26-1.431l1.004-.827c.292-.24.437-.613.43-.992a6.932 6.932 0 010-.255c.007-.378-.138-.75-.43-.99l-1.004-.828a1.125 1.125 0 01-.26-1.43l1.297-2.247a1.125 1.125 0 011.37-.491l1.216.456c.356.133.751.072 1.076-.124.072-.044.146-.087.22-.128.332-.183.582-.495.644-.869l.214-1.281z" />
      <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
    </svg>
  },
  {
    label: 'Logout',
    url: `${AppConfig.APP_URL}/logout`,
    form: {
      action: `${AppConfig.APP_URL}/logout`,
      method: 'POST'
    },
    icon: () => <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
      <path strokeLinecap="round" strokeLinejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
    </svg>  
  }
]

export default function Sidebar() {
  const dispatch: AppDispatch = useDispatch()
  const [active, setActive] = useState('/notification')
  const web = useHistory()
  const location = useLocation()
  const sidebarShow = useSelector(globalStates).sidebarShow
  const [sidebarShowState, setSidebarShowState] = useState("full")

  const handleRoute = (item: MenuType) => {
    if(item.type === 'redirect') {
      document.location.href = item.url
    } else if (item.form) {
      const formEl = document.createElement('form')
      formEl.setAttribute('action', item.form.action)
      formEl.setAttribute('method', item.form.method)
      
      document.body.appendChild(formEl)
      formEl.submit()
    } else {
      web.push(item.url || '/')
    }

  }

  const _handleToggleSidebar = (type: "enter" | "leave") => {
    if (type == "enter" && sidebarShow == "icon") {
      setSidebarShowState("icon")
      dispatch(globalActions.toggleSidebarShow())
    } else if(type == "leave" && sidebarShow == "full" && sidebarShowState == "icon") {
      setSidebarShowState("full")
      dispatch(globalActions.setSidebarShow(sidebarShowState as never))
    }
  }

  useEffect(() => {
    const locationPathname = location.pathname;

    const pathSegments = locationPathname.split("/").filter(segment => segment.trim() !== "")[0];
    setActive(location.pathname == '/' ? '/notification': pathSegments)
  }, [location.pathname])
  
  return (
    <AnimatePresence>
      {
       ( sidebarShow === "icon") &&
        <motion.div
        initial={{ width: 210 }}
        animate={{ width: 47 }}
        exit={{ width: 210 }}
        transition={{ duration: 0.3 }}
        className={`overflow-hidden flex-shrink-0`}
        onMouseEnter={() => _handleToggleSidebar("enter")}
        onMouseLeave={() => _handleToggleSidebar("leave")}
        >
          <div className="border-r border-b rounded-br-lg w-[210px] h-[100vh] flex-shrink-0 sticky bg-white fixed left-0 top-0">
            <div className='p-4'>
              <img src={logoPng} className="w-[160px] h-auto translate-x-[-4%]" />
            </div>
            <ul className="text-sm">
              {
                menus.map((item: MenuType) => {
                  return (
                    <li key={item.label} onClick={() => handleRoute(item)} className={`px-4 py-4 flex space-x-3 hover:active-side-menu ${active == item.url ? 'active-side-menu':'text-gray-600'}`}>
                      {item.icon()}
                      <span>
                        {item.label}
                      </span>
                    </li>
                  )
                })
              }
            </ul>
          </div>
        </motion.div>
      }
      {
        sidebarShow === "full" &&
        <motion.div
        initial={{ width: 210 }}
        animate={{ width: 210 }}
        onMouseLeave={() => _handleToggleSidebar("leave")}
        className={`overflow-hidden flex-shrink-0`}>
          <div className="border-r border-b rounded-br-lg w-[210px] h-[100vh] flex-shrink-0 sticky bg-white fixed left-0 top-0">
            <div className='p-4'>
              <img src={logoPng} className="w-[160px] h-auto translate-x-[-4%]" />
            </div>
            <ul className="text-sm">
              {
                menus.map((item: MenuType) => {
                  return (
                    <li key={item.label} onClick={() => handleRoute(item)} className={`px-4 py-3 flex space-x-3 hover:active-side-menu ${active == item.url ? 'active-side-menu':'text-gray-600'}`}>
                      {item.icon()}
                      <span>
                        {item.label}
                      </span>
                    </li>
                  )
                })
              }
            </ul>
          </div>
        </motion.div>
      }
    </AnimatePresence>
  )
}
