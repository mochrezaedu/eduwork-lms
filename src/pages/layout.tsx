import Router from "../routes/router";

export default function Layout() {
  return (
    <>
      <Router />
    </>
  );
}