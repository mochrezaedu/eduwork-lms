import { isNoteLabel, isStudent } from "@/src/app/types/model";
import Menu from "@/src/components/menu/menu";
// import { openMyModal } from "@/src/components/modal/slice-modal";
// import { MouseEvent } from "react";
// import { useHistory } from "react-router-dom";
// import { useDispatch } from "react-redux";

export default function StudentItem(student: isStudent) {
  const {name,progress,mentor_mandatory,mentor_progress,notes} = student
  const notesCount = notes?.length ?? 0
  // const web = useHistory()

  // const handleOpenNotes = (e: MouseEvent) => {
  //   // const dispatch = useDispatch()
  //   // dispatch(openMyModal);
  //   web.push('/student/notes')
  // }

  return (  
    <tr className="hover:table-active">
      <td className="">
        <input type="checkbox" name="" id="" className="bg-blue-700 mt-1 border-gray-300 rounded-lg" />
      </td>
      <td className="text-sm">{name}</td>
      <td className="text-sm">
        <div className="mb-1">{progress}%</div>
        <div className="w-full flex items-center space-x-2">
          <div className="w-full h-2 bg-gray-200 rounded-full">
            <div className="h-full bg-red-500 rounded-full" style={{ width: `${progress}%` }}></div>
            <div className="h-full bg-blue-500 rounded-full -mt-2" style={{ width: `${mentor_progress}%` }}></div>
          </div>
          {
            mentor_mandatory &&
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={3.5} stroke="currentColor" className="w-4 h-4 text-red-700">
              <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z" />
            </svg>
          }
        </div>
      </td>
      <td>
        <div className="w-full flex items-stretch space-x-1">
          {
            notes?.slice(0,3).map(({label,color,id}:isNoteLabel) => {
              const _color = color?.replace(/ /g, "") ? `[${color}]`: 'gray-600'
              return (
                <span key={id} className={`badge border border-${_color} text-${_color}`}>{label?.value}</span>
              )
            })
          }
          {/* <span className="badge badge-success">Kreatif</span>
          <span className="badge badge-danger">Egois</span> */}
          {
            notesCount > 3 &&
            <>
              <Menu trigger={
                <span className="badge badge-default flex items-center justify-center">
                  +{notesCount - 3}
                </span>
              }
              className="w-[400px]"
              >
                <div className="">
                  {
                    notes?.map(({label,color,id}:isNoteLabel) => {
                      const _color = color?.replace(/ /g, "") ? `[${color}]`: 'gray-600'
                      return (
                        <span key={id} className={`badge inline-block border mb-4 border-${_color} text-${_color}`}>{label?.value}</span>
                      )
                    })
                  }
                </div>
              </Menu>
            </>
          }
          <span className="badge badge-default flex items-center justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
              <path strokeLinecap="round" strokeLinejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
            </svg>
          </span>
        </div>
      </td>
      <td className="space-x-1 flex items-center">
        <a href={`https://wa.me/send?phone=62${student?.phone ? student.phone.replace(/08$/, ''): ''}`} target="_blank" className="btn bg-[#29CC6A] text-white rounded-lg">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={2.5} stroke="currentColor" className="w-3 h-3">
            <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 6.75c0 8.284 6.716 15 15 15h2.25a2.25 2.25 0 002.25-2.25v-1.372c0-.516-.351-.966-.852-1.091l-4.423-1.106c-.44-.11-.902.055-1.173.417l-.97 1.293c-.282.376-.769.542-1.21.38a12.035 12.035 0 01-7.143-7.143c-.162-.441.004-.928.38-1.21l1.293-.97c.363-.271.527-.734.417-1.173L6.963 3.102a1.125 1.125 0 00-1.091-.852H4.5A2.25 2.25 0 002.25 4.5v2.25z" />
          </svg>
        </a>
        <button type="button" className="btn bg-[#3F85EC] text-white rounded-lg">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={2.5} stroke="currentColor" className="w-3 h-3">
            <path strokeLinecap="round" strokeLinejoin="round" d="M12 6.042A8.967 8.967 0 006 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 016 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 016-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0018 18a8.967 8.967 0 00-6 2.292m0-14.25v14.25" />
          </svg>
        </button>
      </td>
    </tr>
  )
}