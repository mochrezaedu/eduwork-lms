import Menu from "@/src/components/menu/menu";

export default function StudentToolbarSort () {
  return (
    <Menu
    className="w-[160px]"
    origin="bottom-right"
    trigger={
      <button type="button" className="px-2 py-1 border border-blue-500 rounded-full text-blue-500 text-xs flex items-center justify-center space-x-1 flex-shrink-0">
        <span>All Sort</span>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
        </svg>
      </button>
    }>
      <div className="w-full divide-y space-y-1">
        <div className="flex items-center w-full space-x-2 pt-1">
          <div className="flex-shrink-0 w-full">
            {
              tableSorts.map(item => {
                return (
                  <div key={item.name} className="flex items-center space-x-2 hover:bg-gray-100 cursor-pointer rounded-lg px-2">
                    <div className="flex-shrink-0">
                      <input type="checkbox" name="" id="" className="w-3 h-3" />
                    </div>
                    <div className="text-xs">{item.label}</div>
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    </Menu>
  )
}

const tableSorts = [
  {
    label: 'Created At',
    name: 'created_at',
  },
  {
    label: 'Last Activity',
    name: 'last_activity',
  }
]