import { allClasses } from "@/src/app/services/class/class-slice";
import Menu from "@/src/components/menu/menu";
import { Autocomplete, InputBase, Popper } from "@mui/material";
import { SyntheticEvent, useState } from "react";
import { useSelector } from "react-redux";

export default function StudentToolbarFilter () {
  const classes = useSelector(allClasses)

  const [selectedOption,setSelectedOption] = useState<Record<any,any> | null>(null);
  const _handleClassInput = (e: SyntheticEvent<Element, Event>, v: Record<any,any> | null) => {
    setSelectedOption(v)
  }
  return (
    <Menu
    className="w-[260px]"
    origin="bottom-right"
    trigger={
      <button type="button" className="px-2 py-1 border border-blue-500 rounded-full text-blue-500 text-xs flex items-center justify-center space-x-1 flex-shrink-0">
        <span>All Filter</span>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
        </svg>
      </button>
    }>
      <div className="w-full divide-y space-y-1">
        <div className="flex items-center w-full space-x-2 pt-1">
          <div className="flex-shrink-0 w-full">
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={classes}
              className="w-full text-xs"
              size="small"
              PopperComponent={ (props) =>
                <Popper {...props} placement="bottom-start" className="bg-white rounded-lg border border-gray-200 text-xs divide-y space-y-1 p-0">
                  <div style={{ border: '1px solid #ccc', padding: '4px' }}>
                    {
                      classes.map((item) => (<div key={item.id} className="hover:bg-gray-100">{item.title}</div>))
                    }
                  </div>
                </Popper>
              }
              sx={
                {
                  "& *": {
                    fontSize:11
                  },
                  "& input": {
                    padding: 0
                  },
                  "& .MuiAutocomplete-endAdornment": {
                    transform: 'translateY(40%)'
                  }
                }
              }
              value={selectedOption}
              onChange={_handleClassInput}
              getOptionLabel={(item:Record<any,any>) => item.title}
              renderInput={(params) => {
                const {InputLabelProps,InputProps,...rest} = params
                return (
                  <InputBase {...params.InputProps} {...rest} placeholder="-- Choose Class --" className="appearance-none h-[24px] border rounded w-full py-0 px-3 text-gray-700 focus:outline-none focus:shadow-outline text-xs flex items-center" />
                )
              }}
            />
          </div>
        </div>
      </div>
    </Menu>
  )
}