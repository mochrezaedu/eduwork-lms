import StudentToolbarFilter from "./student-toolbar-filter"
import StudentToolbarSort from "./student-toolbar-sort"

export default function StudentToolbar() {
  return (
    <div className="flex items-center space-x-3 w-full">
      <div className="text-gray-500">
        Filter
      </div>
      <div className="flex items-center border rounded-full border-blue-500 flex-grow">
        <div className="mx-2 ml-3 rounded-circle">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5 text-blue-500">
            <path strokeLinecap="round" strokeLinejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
          </svg>
        </div>
        <input type="text" name="" id="" className="border-0 rounded-full py-1 outline-none px-2 text-xs w-full" placeholder="Cari Siswa" />
      </div>
      <StudentToolbarFilter />
      <StudentToolbarSort />
    </div>
  )
}