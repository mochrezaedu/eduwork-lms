import axios from "axios"
import { useEffect, useState } from "react"
import StudentItem from "./student-item"
import { isStudent } from "@/src/app/types/model"
import { Collection } from "@/src/utils/collection"
import Skeleton from "@/src/components/skeleton/skeleton"
import StudentToolbar from "./student-toolbar"

export default function Student() {
  const [students,setStudents] = useState<isStudent[]>([])
  const [loading,setLoading] = useState(true);
  const [activeTab,setActiveTab] = useState('unprocessed')

  const getStudents = async() => {
    const result = await axios.get('student/all')
    setStudents([...(await result).data])
    await setLoading(false)
  }

  useEffect(() => {
    getStudents()
  }, [])
  
  return (
    <div className="space-y-4">
      <div className="flex items-center space-x-3 px-3 w-full">
        <div className="text-gray-500">
          Status
        </div>
        {
          !loading &&
          memberStatusses.map(({ label,total }) => {
            return (
              <div key={label} className="relative flex-shrink-0 text-xs">
                <button type="button" className={`px-2 py-1 border rounded-full ${activeTab == label ? 'bg-blue-500 text-white': 'border-blue-500 text-blue-500 bg-blue-100'} hover:bg-blue-500 hover:text-white text-xs flex items-center justify-center space-x-1 flex-shrink-0`} onClick={() => setActiveTab(label)}>
                  <span>{label}</span>
                  <span className="badge badge-success bg-green-50 rounded-full">
                    {total}
                  </span>
                </button>
              </div>
            );
          })
        }
        {
          loading &&
          (new Collection([]).createId(10)).map((item) => {
            return (
              <Skeleton key={item.id} className="px-8 py-3 rounded-full" />
            )
          })
        }
      </div>
      <div className="space-y-5 p-4 bg-white rounded-lg">
        <StudentToolbar />
        <table className="table w-full text-xs">
          <thead className="">
            <tr className="">
              <td className="">
                <input type="checkbox" name="" id="" className="bg-blue-700 mt-1 border-gray-300 rounded-lg" />
              </td>
              <td>Student</td>
              <td>Learning Progress</td>
              <td>Monitor Notes</td>
              <td className="">Action</td>
            </tr>
          </thead>
          <tbody>
            {
              loading &&
              (new Collection([]).createId(10)).map(item => {
                return (
                  <tr key={item.id}>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td className="space-y-2">
                      <Skeleton className="rounded-lg w-full h-6" />
                      <Skeleton className="rounded-lg w-full h-6" />
                    </td>
                  </tr>
                )
              })
            }
            {
              students.map((item: isStudent) => {
                return <StudentItem key={item.id} {...item} />
              })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

const memberStatusses = [
  {
    label: 'unprocessed',
    total: 101,
  },
  {
    label: 'pool',
    total: 77,
  },
  {
    label: 'approved',
    total: 43,
  },
  {
    label: 'penyaluran',
    total: 63,
  },
  {
    label: 'done',
    total: 3,
  },
]