import { isClass } from "@/src/app/types/model"

export default function ClassItem(props: isClass) {
  const {id,title,updated_at_humans,registrants_count,events_count} = props
  return (
    <tr key={id}>
      <td>{title}</td>
      <td>
        <div className="flex space-x-1">
          <div className="badge badge-fill-default">Registrant {registrants_count}</div>
          <div className="badge badge-fill-default">Event {events_count}</div>
        </div>
      </td>
      <td>{updated_at_humans}</td>
      <td>
        <button type="button" className="btn btn-success btn-xs">Lihat</button>
      </td>
    </tr>
  )
}