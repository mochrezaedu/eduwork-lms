import { ActionReducerMapBuilder, PayloadAction, createSlice } from '@reduxjs/toolkit'
import { fetchAllClassHandler } from './api-class'
import { PageClassState } from "@/src/app/types/pages/class";
import { isClass } from '@/src/app/types/model'

const initialState: PageClassState = {
  list: [],
  loading: [],
  error: []
}

export const classSlice = createSlice({
  name: 'class',
  initialState,
  reducers: {
    allClass: (state, action: PayloadAction<isClass>) => {
      console.log(state)
      // state.list = action.payload
    },
  }, 
  extraReducers: (builder: ActionReducerMapBuilder<PageClassState>) => {
    fetchAllClassHandler(builder)
  }
})

// Action creators are generated for each case reducer function
export const { allClass } = classSlice.actions

export default classSlice