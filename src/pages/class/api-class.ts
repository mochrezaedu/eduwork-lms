import { ActionReducerMapBuilder } from "@reduxjs/toolkit";
import { apiClient } from "../../utils/api-client";
import { PageClassState } from "@/src/app/types/pages/class";


export const fetchAllClass = apiClient('class/fetchAllClass', '/class/all')
export const fetchAllClassHandler = (builder: ActionReducerMapBuilder<PageClassState>) => {
  builder
    .addCase(fetchAllClass.pending, (state) => {
      // state.isLoading = true;
      // state.error = null;
    })
    .addCase(fetchAllClass.fulfilled, (state, action) => {
      // state.isLoading = false;
      // state.classes = action.payload;
    })
    .addCase(fetchAllClass.rejected, (state, action) => {
      // state.isLoading = false;
      // state.error = action.payload;
    });
};