import axios from "axios"
import { useEffect, useState } from "react"
import ClassItem from "./class-item"
import { isClass } from "@/src/app/types/model"
import { Collection } from "@/src/utils/collection"
import Skeleton from "@/src/components/skeleton/skeleton"
import { config } from "@/src/app/config"

export default function Class() {
  const [classes, setClasses] = useState<isClass[]>([])
  const [loading,setLoading] = useState(true)

  useEffect(() => {
    const getClasses = async () => {
      const result = await axios.get('class/all')
      setClasses([...(await result).data])
      setLoading(false)
    }
    getClasses()
  }, [])

  return (
    <div className="p-4 space-y-4 bg-white rounded-lg">
      <a href={`${config.APP_URL}/mentor/class/add`} className="btn btn-primary btn-xs">Add Class</a>
      <table>
        <thead>
          <tr>
            <td>Kelas</td>
            <td>Informasi</td>
            <td>Di Update</td>
            <td>Aksi</td>
          </tr>
        </thead>
        <tbody>
          {
            loading &&
            (new Collection([]).createId(10)).map(item => {
              return (
                <tr key={item.id}>
                  <td><Skeleton className="rounded-lg w-full h-6" /></td>
                  <td><Skeleton className="rounded-lg w-full h-6" /></td>
                  <td><Skeleton className="rounded-lg w-full h-6" /></td>
                  <td className="space-y-2">
                    <Skeleton className="rounded-lg w-full h-6" />
                  </td>
                </tr>
              )
            })
          }
          {
            classes.map((item: isClass) => <ClassItem key={item.id} {...item} />)
          }
        </tbody>
      </table>
    </div>
  )
}