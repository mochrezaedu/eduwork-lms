import { fetchNavbarEvent } from "@/src/app/services/event/api"
import { getEventLoads, stickyNavbarEvent } from "@/src/app/services/event/event-slice"
import { isEvent } from "@/src/app/services/event/event-type"
import { AppDispatch } from "@/src/app/types/redux"
import Skeleton from "@/src/components/skeleton/skeleton"
import { Collection } from "@/src/utils/collection"
import { useParams } from "@/src/utils/router"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { ExtractRouteParams } from "react-router"

const LoadingComponent = () => <Skeleton className="w-[25%] h-4 rounded-full" />

export default function EventNavbar() {
  const params: ExtractRouteParams<string, any> = useParams("/event/:id/progress")
  const dispatch: AppDispatch = useDispatch()
  const loads = useSelector(getEventLoads)
  const navbarState = useSelector(stickyNavbarEvent)
  const stickyCount = navbarState?.per_page
  const stickyData = navbarState?.data
  const activeNavbar = stickyData?.find(item => item.id == params.id)

  useEffect(() => {
    dispatch(fetchNavbarEvent(fetchNavbarEvent({
      payload: {
        per_page: navbarState?.per_page
      }
    }))) 
  },[dispatch])

  return (
    <div className="w-full bg-white flex">
      <h5 className="font-bold p-4 flex-shrink-0">Event On Progress</h5>
      <div className="flex-grow flex items-center overflow-hidden hover:overflow-x-auto text-xs small-scroll relative">
        {
          loads.includes("fetch_navbar") &&
          <div className="space-x-2 flex items-center flex-grow">
            {(new Collection([]).createId(stickyCount ?? 3)).map((item) => {
              return (
                <LoadingComponent key={item.id} />
              )
            })}
          </div>
        }
        {
          activeNavbar &&
          <a className={`flex-shrink-0 m-4 hover:active-nav-list active-nav-list`}>{activeNavbar.title}</a>
        }
        {
          !loads.includes("fetch_navbar") &&
          stickyData?.filter(item => item.id != params.id).map((item: isEvent) => {
            return (
              <a key={item.id} className={`flex-shrink-0 m-4 hover:active-nav-list`}>{item.title}</a>
            )
          })
        }
        {/* {
          l.map((item) => {
            return (
              <a key={item.label} className={`flex-shrink-0 m-4 hover:active-nav-list ${active == item.label ? 'active-nav-list': 'text-gray-600'}`}>{item.label}</a>
            )
          })
        } */}
      </div>
    </div>
  )
}