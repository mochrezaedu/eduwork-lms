import { destroyEvent, startEvent } from "@/src/app/services/event/api";
import { isEvent } from "@/src/app/services/event/event-type";
import { AppDispatch } from "@/src/app/types/redux";
import DialogConfirmation from "@/src/components/dialog/dialog-confirmation";
import { useState } from "react";
import { createPortal } from "react-dom";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

export default function EventItem({item}:{item:isEvent}) {
  const dispatch: AppDispatch = useDispatch()
  const web = useHistory()
  const [openConfirmation,setOpenConfirmation] = useState(false)
  const [openConfirmationDelete,setOpenConfirmationDelete] = useState(false)

  const _handleStartEvent = async() => {
    setOpenConfirmation(true)
  }
  const _handleIsConfirmedStartEvent = () => {
    dispatch(startEvent({
      payload: {
        id: item.id
      }
    }))
    web.push(`/event/${item.id}/progress`)
  }
  const _handleIsConfirmedDestroyEvent = () => {
    dispatch(destroyEvent({
      payload: {
        id: item.id
      }
    }))
  }

  return (
    <tr>
      <td></td>
      <td className="text-left">{item.title}</td>
      <td>{item.type}</td>
      <td>{item.e_date}</td>
      <td></td>
      <td className="flex flex-col space-y-2">
        {
          item.status == null &&
          <>
            <button type="button" className="btn btn-success btn-xs" onClick={_handleStartEvent}>Start</button>
            {
              openConfirmation &&
              createPortal(
                <DialogConfirmation onClose={() => setOpenConfirmation(false)} onSubmit={_handleIsConfirmedStartEvent} open={openConfirmation} title="Yakin ingin memulai event ?" btnSubmitText={'Start'}>
                  Dengan klik start akan membuat TODO List yang harus diselesaikan!
                </DialogConfirmation>,
                document.body
              )
            }
          </>
        }
        {
          item.status == "progress" &&
          <button type="button" className="btn btn-default btn-xs" onClick={() => web.push(`/event/${item.id}/progress`)}>Lihat</button>
        }
        <button type="button" className="btn btn-primary btn-xs" onClick={() => web.push(`/event/${item.id}/update`, {modal: true})}>Update</button>
        {
          openConfirmationDelete &&
          createPortal(
            <DialogConfirmation onClose={() => setOpenConfirmationDelete(false)} onSubmit={_handleIsConfirmedDestroyEvent} open={openConfirmationDelete} title="Yakin ingin hapus event ?" btnSubmitText={'Hapus'} btnSubmitType="danger">
              Dengan klik hapus akan menghapus event!
            </DialogConfirmation>,
            document.body
          )
        }
        <button type="button" className="btn btn-danger btn-xs" onClick={() => setOpenConfirmationDelete(true)}>Hapus</button>
        {
          openConfirmationDelete &&
          createPortal(
            <DialogConfirmation onClose={() => setOpenConfirmationDelete(false)} onSubmit={_handleIsConfirmedDestroyEvent} open={openConfirmationDelete} title="Yakin ingin hapus event ?" btnSubmitText={'Hapus'} btnSubmitType="danger">
              Dengan klik hapus akan menghapus event!
            </DialogConfirmation>,
            document.body
          )
        }
      </td>
    </tr>
  )
}