// import { useEffect } from "react"
// import { useDispatch } from "react-redux"

import { fetchAllEvent } from "@/src/app/services/event/api"
import { eventActions, eventStates } from "@/src/app/services/event/event-slice"
import { AppDispatch } from "@/src/app/types/redux"
import Skeleton from "@/src/components/skeleton/skeleton"
import { Collection } from "@/src/utils/collection"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import EventItem from "./event-item"
import Modal from "@/src/components/modal/modal"
import { createPortal } from "react-dom"
import EventCreate from "./event-create"

export default function Event() {
  const dispatch: AppDispatch = useDispatch()
  const eventState = useSelector(eventStates)
  const perPage = eventState.per_page
  const loads = eventState.loads
  const [openCreateModal, setOpenCreateModal] = useState(false)

  useEffect(() => {
    return () => {
      dispatch(eventActions.cleanupEvent())
    }
  }, [])

  useEffect(() => {
    dispatch(fetchAllEvent({
      payload: {
        per_page: perPage
      }
    }))
  }, [dispatch])

  return (
    <>
      <div className="space-y-5 p-4 bg-white rounded-lg">
        <button type="button" className="btn btn-primary btn-xs" onClick={() => setOpenCreateModal(true)}>Add Event</button>

        <table className="table w-full text-xs">
          <thead className="">
            <tr className="">
              {/* <td className="">
                <input type="checkbox" name="" id="" className="bg-blue-700 mt-1 border-gray-300 rounded-lg" />
              </td> */}
              <td></td>
              <td>Title</td>
              <td>Type</td>
              <td>Date</td>
              <td>Information</td>
              <td className="">Action</td>
            </tr>
          </thead>
          <tbody>
            {
              loads.includes("fetch_all") &&
              (new Collection([]).createId(perPage)).map(item => {
                return (
                  <tr key={item.id}>
                    <td></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td><Skeleton className="rounded-lg w-full h-6" /></td>
                    <td className="space-y-2">
                      <Skeleton className="rounded-lg w-full h-6" />
                      <Skeleton className="rounded-lg w-full h-6" />
                    </td>
                  </tr>
                )
              })
            }
            {
              !loads.includes("fetch_all") &&
              eventState.data.map(item => <EventItem key={item.id} item={item} />)
            }
          </tbody>
        </table>
      </div>
      {
        createPortal(
          <Modal closable={false} open={loads.includes("start_event")} className="h-auto">
            <div className="text-center">Starting Event</div>
          </Modal>,
          document.body
        )
      }
      {
        createPortal(
          <Modal className="" title={'Create Event'} open={openCreateModal} onClose={() => setOpenCreateModal(false)}>
            <EventCreate />
          </Modal>,
          document.body
        )
      }
    </>
  )
}