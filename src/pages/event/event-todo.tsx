// import { fetchTodoEvent } from "@/src/app/services/notification/api";
import { getLoads, notificationActions, notificationEventProgress } from "@/src/app/services/notification/notification-slice";
import { isNotification } from "@/src/app/services/notification/notification-type";
import { AppDispatch, RootState } from "@/src/app/types/redux";
import ButtonCheck from "@/src/components/todo/button-check";
import Todo from "@/src/components/todo/todo";
import TodoCopyUrl from "@/src/components/todo/todo-copy-url";
import TodoInputLinkRecord from "@/src/components/todo/todo-input-link-record";
import TodoReviewStudent from "@/src/components/todo/todo-review-student";
import { Collection } from "@/src/utils/collection";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import EventMembers from "./event-members";
import { useEffect } from "react";
import { fetchTodoEvent, readNotification } from "@/src/app/services/notification/api";
import EventTodoNotFound from "../error/event-todo-not-found";

export default function EventProgress() {
  const dispatch: AppDispatch = useDispatch()
  const params: Record<string, any> = useParams()
  const allTodoEventState = useSelector((state: RootState) => notificationEventProgress(state,params.id))
  const allTodoEvent = allTodoEventState
  const loads = useSelector(getLoads)

  const _handleCheck = (id: number) => {
    dispatch(notificationActions.readNotification(id))
    dispatch(readNotification({
      payload: {
        id
      }
    }))
  }

  useEffect(() => {
    dispatch(fetchTodoEvent({
      payload: {
        id: params.id
      }
    }))
  }, [dispatch])

  return (
    <div className="flex space-x-6 px-10 items-start">
      <EventMembers eventId={params.id} />
      <div className="space-y-5 flex-grow">
        {
          loads.includes('fetch_todo') &&
          (new Collection([])).createId(4).map((item) => {
            return (
              <Todo 
                key={item.id}
                loading={true}
                title={<>test</>}
                description={<>test description</>}
                action={<ButtonCheck loading={true} onClick={() => {}} />}
              />
            )
          })
        }
        {
          !loads.includes('fetch_todo') &&
          allTodoEvent.map((item: isNotification) => {
            return (
              <div key={item.id}>
                {
                  item.type == 'COPY_LINK' &&
                  <TodoCopyUrl urlToCopy={`https://eduwork.id/event/${item.url_redirect}`} title={<>{item.title}</>} description={item.description} action={<ButtonCheck onClick={() => {_handleCheck(item.id)}} />} priority={item.priority_text} />
                }
                {
                  item.type == 'INPUT_LINK_RECORD' &&
                  <TodoInputLinkRecord label={"INPUT LINK REKAMAN"} data={item} urlToCopy={`https://eduwork.id/event/${item.url_redirect}`} title={<>{item.title}</>} description={item.description} priority={item.priority_text} />
                }
                {
                  item.type == 'REVIEW_STUDENT' &&
                  <TodoReviewStudent data={item} />
                }
                {
                  Boolean(item.type == 'BASIC' || item.type == null) &&
                  <Todo 
                    title={<>{item.title} {item.id}</>}
                    priority={item.priority_text}
                    description={item.description}
                    action={<ButtonCheck onClick={() => {_handleCheck(item.id)}} />}
                  />
                }
              </div>
            )
          })
        }
        {
          Boolean(!loads.includes('fetch_todo') && allTodoEvent.length === 0) &&
          <EventTodoNotFound />
        }
      </div>
    </div>
  )
}