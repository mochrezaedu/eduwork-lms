import { config } from "@/src/app/config";
import { getEventTodoLoads, registrantEventMentorReviews } from "@/src/app/services/event/event-todo-slice";
import { registrantsForMentorReviews } from "@/src/app/services/registrant/api";
import { AppDispatch } from "@/src/app/types/redux";
import Skeleton from "@/src/components/skeleton/skeleton";
import { Collection } from "@/src/utils/collection";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

type EventMembersProps = {
  eventId: number;
}

export default function EventMembers({eventId}: EventMembersProps) {
  const params: Record<string, any> = useParams()
  const dispatch:AppDispatch = useDispatch()

  const registrants = useSelector(registrantEventMentorReviews)
  const loading = useSelector(getEventTodoLoads).includes("fetch_registrants_for_mentor_reviews")

  useEffect(() => {
    dispatch(registrantsForMentorReviews({
      payload: {id:params.id}
    }))
  }, [dispatch])


  return (
    <div className="flow-root bg-white rounded-lg w-[300px] top-0 flex-shrink-0 p-4 border border-blue-200 overflow-y-auto">
      <div className="text-gray-600">Members</div>
      <ul role="list" className="divide-gray-200 w-full divide-y">
        {
          loading &&
          (new Collection([])).createId(10).map(item => {
            return (
              <li className="py-1 sm:py-4 w-full" key={item.id}>
                <div className="flex items-center space-x-4 w-full">
                    <div className="flex-shrink-0">
                      <Skeleton className="w-8 h-8 rounded-full" />
                    </div>
                    <div className="flex-grow flex justify-between items-center">
                        <Skeleton className="rounded-lg w-[135px] h-6" />
                        <Skeleton className="rounded-lg w-16 h-6" />
                    </div>
                </div>
              </li>
            )
          })
        }
        {
          registrants?.map(item => {
            let photoUrl = config.APP_URL
            if (item.student?.photo) {
              photoUrl = photoUrl + "/storage/user/" + item.student?.id + "/" + item.student?.photo
            } else {
              photoUrl = photoUrl + "/images/avatar/noimage.jpg"
            }
            return (
              <li className="py-1 w-full" key={item.id}>
                <div className="flex items-center space-x-4 w-full">
                    <div className="flex-shrink-0">
                      {
                        loading ?
                        <Skeleton className="w-8 h-8 rounded-full" />:
                        <img className="w-[42px] h-[42px] rounded-full object-center object-cover" src={photoUrl} alt="Neil image" />
                      }
                    </div>
                    <div className="flex-grow flex justify-between items-center">
                        {
                          loading &&
                          <Skeleton className="rounded-lg w-[135px] h-6" />
                        }
                        {
                          loading &&
                          <Skeleton className="rounded-lg w-16 h-6" />
                        }
                        {
                          !loading &&
                          <p className="font-semibold text-gray-900 flex-wrap text-xs">
                            {item.student.name}
                          </p>
                        }
                        {
                          Boolean(!loading && item.presenced) &&
                          <div className="py-2 px-3 border rounded-lg text-xs font-bold bg-blue-300">Attended</div>
                        }
                        {/* <div className="flex items-center space-x-1 text-sm break-words w-full text-green-600" style={{overflowWrap: 'anywhere'}}>
                          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z" />
                          </svg>
                          <span className="text-sm">Hadir</span>
                        </div> */}
                    </div>
                </div>
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}