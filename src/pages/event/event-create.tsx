// import { LocalizationProvider, MobileDateTimePicker } from "@mui/x-date-pickers";
import DatePicker from "react-datepicker";
import moment, { Moment } from "moment";
import { ChangeEvent, SyntheticEvent, useEffect, useState } from "react";
// import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from 'draft-js';
import { convertToHTML } from 'draft-convert';

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import 'moment/locale/de';
import Input from "@/src/components/form/input";
import { allClasses } from "@/src/app/services/class/class-slice";
import { useDispatch, useSelector } from "react-redux";
import { Autocomplete, InputBase } from "@mui/material";
import { getEventLoads, eventTypes as getEventTypes } from "@/src/app/services/event/event-slice";
import { AppDispatch } from "@/src/app/types/redux";
import { storeEvent } from "@/src/app/services/event/api";
import "react-datepicker/dist/react-datepicker.css";

export default function EventCreate() {
  const dispatch: AppDispatch = useDispatch()
  const [value] = useState<Moment | null>(null);
  const [forms, setForms] = useState<Record<string, any>>({
    title: '',
    type: '',
    date: null,
    class_id: '',
    description: ''
  })
  const [selectedOption,setSelectedOption] = useState<Record<any,any> | null>(null);
  const classes = useSelector(allClasses)
  const eventTypes = useSelector(getEventTypes)
  const loading = useSelector(getEventLoads).includes("store_event")
  const areAllFieldsFilled = Object.keys(forms).every(key => {
    return key === 'description' || (forms[key] !== '' && forms[key] !== null);
  });

  const [description,setDescription] = useState<EditorState>(EditorState.createEmpty())
  // const [showDate, setShowDate] = useState(false)
  // const dateInputRef = useRef(null)

  const _handleChangeDescription = (e: EditorState) => {
    // console.log  (convertToHTML(e.getCurrentContent()))
    setDescription(e)
  }
  const _handleClassInput = (e: SyntheticEvent<Element, Event>, v: Record<any,any> | null) => {
    setSelectedOption(v)
    // setSelectedOption(e)
  }
  const _handleSubmit = () => {
    if (!loading && areAllFieldsFilled) {
      dispatch(storeEvent({
        payload: {
          ...forms, date: moment(forms.date).format("YYYY-MM-DD hh:mm")
        }
      }))
    }
  }

  useEffect(() => {
    if (value) {
      setForms({...forms, date: value.format('YYYY-MM-DD hh:mm')})
    } else {
      setForms({...forms, date: ''})
    }
  }, [value])

  useEffect(() => {
    if (selectedOption !== null) {
      setForms({...forms, class_id: selectedOption.id})
    } else {
      setForms({...forms, class_id: ''})
    }
  }, [selectedOption])

  useEffect(() => {
    if (description !== null) {
      setForms({...forms, description: convertToHTML(description.getCurrentContent())})
    } else {
      setForms({...forms, description: ''})
    }
  }, [description])
  

  return (
    <div className="w-full border-t mt-4 pt-4">
      <form className="bg-white">
       <Input label="Title" type="text" className="mb-4" onChange={(e: ChangeEvent<HTMLInputElement>) => setForms({...forms, title: e.target.value})} />
        <div className="flex space-x-4 items-end">
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Event Date
            </label>
            {/* <LocalizationProvider dateAdapter={AdapterMoment}>
              <MobileDateTimePicker 
              className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline max-h-[38.46px] [&>*:first-child]:max-h-[38.46px] [&>*:first-child>fieldset]:!border-gray-50 [&>*:first-child]:!border !border-gray-50"
              onChange={setValue}
              ampm={false}
              reduceAnimations={true}
              value={value} />
            </LocalizationProvider> */}
            <DatePicker
              selected={forms.date}
              onChange={(date) => setForms({...forms, date})}
              showTimeSelect
              dateFormat="MMMM d, yyyy h:mm aa"
              timeIntervals={5}
              className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline max-h-[38.46px]"
            />
            {/* <input value={value?.format("YYYY-MM-DD hh:mm")} readOnly onClick={() => setShowDate(!showDate)} className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" placeholder="Y-m-d h:i:s" /> */}
          </div>
          <div className="mb-4 w-full">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Type
            </label>
            <select name="" id="" placeholder="-- Choose Type --" onChange={(e) => setForms({...forms, type: e.target.value})} className="appearance-none border bg-white rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
              <option value="" className="text-gray-500">-- Choose Type --</option>
              {
                eventTypes?.map(item => <option key={item.name} value={item.name}>{item.label}</option>)
              }
            </select>
          </div>
           {/* <Input label="Event Type" type="text" className="mb-4" onChange={(e: ChangeEvent<HTMLInputElement>) => setForms({...forms, type: e.target.value})} /> */}
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Class
          </label>
          <Autocomplete
            disablePortal
            id="combo-box-demo"
            options={classes}
            className="w-full"
            value={selectedOption}
            onChange={_handleClassInput}
            getOptionLabel={(item:Record<any,any>) => item.title}
            renderInput={(params) => {
              const {InputLabelProps,InputProps,...rest} = params
              return (
                <InputBase {...params.InputProps} {...rest} placeholder="-- Choose Class --" className="appearance-none h-[38.18px] border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" />
              )
            }}
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Description
          </label>
          <Editor
            editorState={description}
            toolbarClassName="toolbarClassName"
            wrapperClassName="w-full"
            editorClassName="editorClassName min-h-[300px]"
            onEditorStateChange={_handleChangeDescription}
          />
          {/* <input className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Title" /> */}
        </div>
        <button type="button" className={`btn ${Boolean(!loading && areAllFieldsFilled) ? 'btn-primary': 'bg-gray-300 opacity-75 cursor-default'} text-sm w-full`} onClick={_handleSubmit}>{loading ? "...": "Submit"}</button>
      </form>
    </div>
  )
}