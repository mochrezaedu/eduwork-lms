import { useLocation } from "react-router-dom";

export default function NavbarDefault() {
  const history = useLocation()
  return (
    <div className="w-full bg-white flex">
      <h5 className="text-sm font-bold p-4 flex-shrink-0">{history.pathname.replace("/", "").toUpperCase() || 'Notification'}</h5>
    </div>
  )
}