import NotificationItem from "./notification-item"
import { useDispatch, useSelector } from "react-redux"
import { useEffect } from "react"
import { AppDispatch } from "@/src/app/types/redux"
import { notificationAll, getLoads, notificationStates, cleanupNotification } from "../../app/services/notification/notification-slice"
import { Collection } from "@/src/utils/collection"
import NotificationNotFound from "../error/notification-not-found"
import { fetchAllNotification } from "@/src/app/services/notification/api"
import { isNotification } from "@/src/app/services/notification/notification-type"

export default function Notification() {
  const dispatch: AppDispatch = useDispatch()
  const notifications = useSelector(notificationAll)
  const notificationCount = notifications.length
  const loads = useSelector(getLoads)
  const notificationState = useSelector(notificationStates)
  const perPage = notificationState.per_page;
  const total = notificationState.total

  const handleAutoLoadMore = () => {
    console.log('scroll max')
  }

  const scrollMaxEvent = () => {
    const distanceToBottom = document.documentElement.offsetHeight - (window.scrollY + window.innerHeight);
    
    if (distanceToBottom < 100) {
      if ((total as number) > notificationCount) {
        handleAutoLoadMore();
      }
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', scrollMaxEvent);
    return () => {
      window.removeEventListener('scroll', scrollMaxEvent);
    };
  }, [])

  useEffect(() => {
    dispatch(fetchAllNotification({
      payload: {per_page:perPage}
    }))
    return () => {
      dispatch(cleanupNotification())
    }
  }, [dispatch])

  useEffect(() => {
    
  }, [notifications])

  return (
    <div className="space-y-8">
      {
        loads.length > 0 &&
        (new Collection([]).createId(10)).all().map((item) => {
          return (
            <NotificationItem key={item?.id} loading={true} />
          )
        })
      }
      {
        (loads.length < 1 && notificationCount < 1) &&
        <NotificationNotFound />
      }
      {
        notifications.filter(item => item.type_prefix != 'event').map((item: isNotification) => {
          return (
            // <></>
            <NotificationItem key={item.id} item={item} />
          )
        })
      }
    </div>
  )
}