import { readNotification } from "@/src/app/services/notification/api";
import { notificationActions } from "@/src/app/services/notification/notification-slice";
import { isNotification } from "@/src/app/services/notification/notification-type";
import { AppDispatch } from "@/src/app/types/redux";
import Skeleton from "@/src/components/skeleton/skeleton";
import { useDispatch } from "react-redux";

export default function NotificationItem({item,loading}:{item?: isNotification | null, loading?: Boolean}) {
  const dispatch: AppDispatch = useDispatch()
  
  const _handleCheck = (id?: number) => {
    dispatch(notificationActions.readNotification(id))
    dispatch(readNotification({
      payload: {
        id
      }
    }))
  }
  return (
    <div className="flex items-start w-[750px] mx-auto space-x-3">
      <div className="rounded-lg flex-grow border border-blue-200 hover:shadow-lg">
        <div className="bg-white rounded-t-lg py-3 px-4 space-x-2 flex items-center justify-between w-full">
          <div className="flex items-center space-x-2">
            {
              loading ? 
              <Skeleton className="h-8 rounded w-[100px]" />:
              item?.priority_text &&
              <span className="relative bg-red-100 px-4 py-2 rounded-lg text-sm flex items-center space-x-2">
                <div className="w-3 h-3 rounded-full bg-red-600"></div>
                <span>{item?.priority_text}</span>
              </span>
            }
            {
              loading ?
              <Skeleton />:
              <span>{item?.title}</span>
            }
          </div>
          <button type="button" className="text-blue-500">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
              <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
            </svg>
          </button>
        </div>
        <div className="bg-blue-100 rounded-b-lg py-5 px-4">
          {
            loading ? 
            <Skeleton className="h-8 rounded w-full" />:
            item?.description
          }
        </div>
      </div>
      {
        loading ? 
        <Skeleton className="w-5 h-5 mt-2 rounded-full" />:
        <button type="button" className="btn btn-primary rounded-full flex-shrink-0 mt-2" onClick={() => _handleCheck(item?.id)}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={2.5} stroke="currentColor" className="w-5 h-5">
            <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12.75l6 6 9-13.5" />
          </svg>
        </button>
      }
    </div>
  )
}