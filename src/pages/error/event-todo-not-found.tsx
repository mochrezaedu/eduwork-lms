import { fetchTodoEvent } from "@/src/app/services/notification/api"
import { AppDispatch } from "@/src/app/types/redux"
import { useDispatch } from "react-redux"
import { useParams } from "react-router-dom"

interface EventTodoNotFoundProps {
  onRefresh?: Function
}


export default function EventTodoNotFound({onRefresh = () => {}}: EventTodoNotFoundProps) {
  const dispatch: AppDispatch = useDispatch()
  const params: Record<string, any> = useParams()

  const _handleReload = () => {
    dispatch(fetchTodoEvent({
      payload: {
        id: params.id
      },
      type: 'reload'
    }))
  }


  return (
    <div className="space-y-3 h-[200px] flex flex-col justify-center items-center mx-auto">
      <div className="text-gray-300">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-[50px] h-[50px]">
          <path strokeLinecap="round" strokeLinejoin="round" d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z" />
        </svg>
      </div>
      <div className="text-gray-300 text-lg space-y-1">
        <div>Event Todo Not Found.</div>
        <button className="flex items-center mx-auto hover:shadow rounded-full px-3 py-2" onClick={() => _handleReload()}>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 mr-1">
            <path strokeLinecap="round" strokeLinejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
          </svg>
          Reload  
        </button>
      </div>
    </div>
  )
}