import splashImage from '@/assets/splash.png'

export default function PostLoading({message}:{message?: string}) {
  const percentage = 100  
  return(
    <div className="fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 rounded-lg space-y-6 shadow-lg pb-4">
      <img src={splashImage} className='rounded-t-lg' />
      {
        !message &&
        <>
          <div className='flex flex-col items-center justify-center space-y-3'>
            <div className='font-bold text-2xl text-gray-700'>Please Wait</div>
            <div className='text-lg text-gray-500'>We’ve loaded {percentage}% of your data</div>
          </div>
          <div className='w-[80%] mx-auto'>
            <div className='bg-gray-300 rounded-full h-4'></div>
            <div className={`bg-primary rounded-full h-4 -mt-4`} style={{width: `${percentage}%`}}></div>
          </div>
        </>
      }
      {
        message &&
        <div className='flex flex-col items-center justify-center space-y-3 pb-2'>
          <div className='font-bold text-2xl text-gray-700'>Sedang Mengalihkan...</div>
          <div className='text-lg text-gray-500 mx-auto'>{message}</div>
        </div>

      }
    </div>
  )
}