import { useLocation } from "react-router-dom";
import EventNavbar from "./event/event-navbar";
import NavbarDefault from "./navbar-default";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../app/types/redux";
import { globalActions, globalStates } from "../app/services/global/global-slice";
import { AnimatePresence,motion } from "framer-motion";


export default function Navbar() {
  const history = useLocation()
  const dispatch: AppDispatch = useDispatch()
  const sidebarShow = useSelector(globalStates).sidebarShow

  return (
    <AnimatePresence>
      {
       ( sidebarShow === "icon") &&
        <motion.div
        initial={{ paddingLeft: 210 }}
        animate={{ paddingLeft: 47 }}
        exit={{ paddingLeft: 210 }}
        transition={{ duration: 0.3 }}
        className={`fixed top-0 left-0 w-full pl-[210px] z-[9]`}>
          <div className="w-full flex items-stretch">
            <button type="button" className="px-3 bg-white" onClick={() => dispatch(globalActions.toggleSidebarShow())}>
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
              </svg>
            </button>
            {
              history.pathname.match('/event/*') &&
              <EventNavbar />
            }
            {
              ['/','/class','/student','/notification','/setting','/support'].includes(history.pathname) &&
              <NavbarDefault />
            }
          </div>
        </motion.div>
      }
      {
        sidebarShow === "full" &&
        <motion.div
        initial={{ paddingLeft: 210 }}
        animate={{ paddingLeft: 210 }}
        className={`fixed top-0 left-0 w-full pl-[210px] z-[9]`}>
          <div className="w-full flex items-stretch">
            <button type="button" className="px-3 bg-white" onClick={() => dispatch(globalActions.toggleSidebarShow())}>
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
              </svg>
            </button>
            {
              history.pathname.match('/event/*') &&
              <EventNavbar />
            }
            {
              ['/','/class','/student','/notification','/setting','/support'].includes(history.pathname) &&
              <NavbarDefault />
            }
          </div>
        </motion.div>
      }
    </AnimatePresence>
    
  )
}