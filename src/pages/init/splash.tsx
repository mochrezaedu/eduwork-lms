import splashImage from '@/assets/splash.png'
import { useDispatch, useSelector } from 'react-redux'
import { getLoads, getTotalLoad } from './init-slice'
import { useEffect } from 'react'
import { AppDispatch } from '@/src/app/types/redux'
import { loadAll } from './init-extra'

export default function Splash() {
  const dispatch: AppDispatch = useDispatch()
  const loads = useSelector(getLoads)
  const total = useSelector(getTotalLoad)
  const percentageUnfixed = (((total-(loads.length)) / total) * 100)
  const percentage = percentageUnfixed > 0 && percentageUnfixed < 100 ? percentageUnfixed.toFixed(1): percentageUnfixed
  
  useEffect(() => {
    dispatch(loadAll())
  }, [dispatch])
  
  return(
    <div className="fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 rounded-lg space-y-6 shadow-lg pb-4">
      <img src={splashImage} className='rounded-t-lg' />
      <div className='flex flex-col items-center justify-center space-y-3'>
        <div className='font-bold text-2xl text-gray-700'>Please Wait</div>
        <div className='text-lg text-gray-500'>We’ve loaded {percentage}% of your data</div>
      </div>
      <div className='w-[80%] mx-auto'>
        <div className='bg-gray-300 rounded-full h-4'></div>
        <div className={`bg-primary rounded-full h-4 -mt-4`} style={{width: `${percentage}%`}}></div>
      </div>
    </div>
  )
}