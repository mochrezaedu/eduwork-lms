import { createAsyncThunk } from "@reduxjs/toolkit";
import { removeLoad } from "./init-slice";
import { getAuthUser, setAuthentication } from "@/src/app/services/auth/auth-api";
import { RootState } from "@/src/app/types/redux";
import { AxiosWrapper } from "@/src/utils/axios";

export const loadAll = createAsyncThunk('init/load_all', async (_: void,{dispatch, getState}) => {
  const loaded: string[] = []
  const maxWait = 1000
  const state = await getState() as RootState
  const auth = state.auth
  const token: string = localStorage.getItem('api_token') ?? ''
  AxiosWrapper.init({
    authorization: token
  })  
  await new Promise((resolve) => {
    (setTimeout(resolve, Math.floor(Math.random() * maxWait) + 500))
  }).then(() => {
    dispatch(removeLoad('check_auth'))
  })
  
  await new Promise((resolve) => {
    (setTimeout(resolve, Math.floor(Math.random() * maxWait) + 500))
  }).then(() => {
    if (!auth.user) {
      dispatch(getAuthUser())
      dispatch(setAuthentication())
    }
    dispatch(removeLoad('store_auth'))
    dispatch(removeLoad('get_auth'))
  })

  return loaded
})