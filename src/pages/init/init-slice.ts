import { initPageInterface } from "@/src/app/types/pages/init";
import { RootState } from "@/src/app/types/redux";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState: initPageInterface = {
  loads: ['check_auth','get_auth','store_auth', 'fetch_event_types', 'fetch_classes'],
  total: 5
}

const initSlice = createSlice({
  name: 'init',
  initialState,
  reducers: {
    storeLoad: (state, action: PayloadAction) => {
      console.log(state)
    },
    removeLoad: (state, action: PayloadAction<string>) => {
      const newLoads = state.loads.filter(item => item != action.payload)
      state.loads = newLoads
    }
  }
})

export const {
  removeLoad
} = initSlice.actions

export const getLoads = (state: RootState) => state.initPage.loads
export const getTotalLoad = (state: RootState) => state.initPage.total

export default initSlice