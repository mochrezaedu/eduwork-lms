import { matchPath, useLocation } from "react-router-dom"

export function useParams(path: string) {
  const { pathname } = useLocation()
  const match = matchPath(pathname, { path })
  return match?.params || {}
}