import axios from "axios";
import {config as AppConfig} from '../app/config'

interface AxiosConfigInterface {
  baseUrl?: string | null;
  authorization: string;
  contentType?: string;
}

export const AxiosWrapper = {
  init: (config?: AxiosConfigInterface) => {
    axios.defaults.baseURL = AppConfig.API_URL;
    if (config?.authorization) {
      axios.defaults.headers.common['Authorization'] = `bearer ${config.authorization}`;      
    }
    // axios.defaults.headers.post['Content-Type'] = config.contentType ?? 'application/json';
  }
}