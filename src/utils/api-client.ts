import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const defaultApiClient = (name: string, url: string, config = {}) => {
  return createAsyncThunk(name, async (_, { rejectWithValue }) => {
    try {
      // const response = await axios(config)
      const response = await fetch(url, config);
      const data = await response.json();
      return data;
    } catch (error: any) {
      return rejectWithValue(error || 'An error occurred');
    }
  });
};

export const apiClient = (name: string, url: string, config = {}) => {
  const api_token = localStorage.getItem('api_token')
  return createAsyncThunk(name, async (_, { rejectWithValue }) => {
    try {
      const response = await axios({
        ...config,
        url,
        headers: {
          "Authorization": `bearer ${api_token}`
        }
      });
      const data = await response.data;
      return data;
    } catch (error: any) {
      return rejectWithValue(error.message || 'An error occurred');
    }
  });
};

// export const getApi = async(url: string, config: AxiosRequestConfig | {}) => {
//   const response = await axios.get(url, config);
//   const data = await response.data;
//   return data;
// };

// export const postApi = (url: string, params: Object, config: AxiosRequestConfig | {}) => {
//   try {
//     const response = await axios.post(url, params, config);
//     const data = await response.data;
//     return data;
//   } catch (error: any) {
//     return rejectWithValue(error.message || 'An error occurred');
//   }
// };