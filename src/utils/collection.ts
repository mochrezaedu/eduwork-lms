import {Collection as BaseCollection} from 'collect.js'

export class Collection extends BaseCollection<any> {
  constructor(args: [] | {}) {
    super(args)
  }
  createId(count: number) {
    for (let index = 0; index < count; index++) {
      this.push({id: index})
    }
    return this
  }
}

const collect = (args: Array<String | Number>) => {
  return new Collection(args)
}

export {collect}

export default collect