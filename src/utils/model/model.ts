import * as ModelSetter from './model-setter'
import * as ModelGetter from './model-getter'

class Model {
  constructor(path: string) {
    this.path = path
    this.selectors = Model.prototype.selectors = {
      all: ModelGetter.all.bind(this)
    }
  };
  public path: string;
  public actions?: any;
  public selectors?: any;
}

Model.prototype.actions = ModelSetter

export default Model