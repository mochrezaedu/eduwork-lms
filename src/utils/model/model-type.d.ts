import { ObjectTypeDeclaration } from "typescript";
import Model from "./model";

export type ModelType = Model<T>

export type ModelActions = {
  create: () => {}
}