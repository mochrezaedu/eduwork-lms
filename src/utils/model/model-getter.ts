import Model from "./model"
import { getStoreState } from "./model-helper";

export const all = (state: any, model: Model): Array<Object> => {
  console.log(model)
  const currentRows = getStoreState(model,state)?.data

  return currentRows;
}