import { Draft } from "@reduxjs/toolkit"
import Model from "./model"

export const getStoreState = (model: Model,state: Draft<any>) => {
  const prefixes = model.path.split('.');
  let stateStore: any = null;
  
  prefixes.map((item,index) => {
    if(stateStore === null) {
      stateStore = state[item]
    } else if(stateStore !== false) {
      stateStore = stateStore[item]
    } else {
      stateStore = false;
    }
  })

  if(!stateStore) {
    throw 'Store path not found.'
  }

  return stateStore
}