import { Draft, PayloadAction } from '@reduxjs/toolkit';

export const removeLoad = <T extends { loads: string[] }>(state: Draft<T>, action: PayloadAction<string>) => {
  const newLoads = state.loads.filter((item) => item !== action.payload);
  state.loads = newLoads;
  return state;
};

export const addLoad = <T extends { loads: string[] }>(state: Draft<T>, action: PayloadAction<string>) => {
  const newLoads = state.loads.find((item) => item === action.payload);
  if (!newLoads) {
    state.loads = [...state.loads, action.payload];
  }
  return state;
};

type createOrUpdateActionType = {
  excludeLoad: string;
  model: Array<Record<any,any>>;
}

export const createOrUpdate = <T extends Record<any, any>>(state: Draft<T>, action: PayloadAction<createOrUpdateActionType>) => {
  const {payload} = action
  const {model,excludeLoad} = payload
  const newIds: Array<number> = model.map((item:Record<any,any>) => item.id)
  const removedData = state.data.filter((item:Record<any,any>) => !newIds.includes(item.id))
  const newData = [...removedData, ...model]
  const newLoads = state.loads.filter((item:string) => item != excludeLoad)


  const newState = {...state,data: newData, loads: newLoads} 
  return newState
}

export const BaseReducer = {
  removeLoad,addLoad,createOrUpdate
}

export default BaseReducer