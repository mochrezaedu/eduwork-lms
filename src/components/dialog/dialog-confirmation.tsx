import { FC, ReactNode } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { ClickAwayListener } from '@mui/material';

interface ConfirmBoxProps {
  children: ReactNode;
  closable?: Boolean;
  open: Boolean;
  title?: string;
  onClose: Function;
  onSubmit: Function;
  btnSubmitText?: string;
  btnSubmitType?: string;
}

const DialogConfirmation: FC<ConfirmBoxProps> = ({ children, open, title, onSubmit, onClose, btnSubmitText, btnSubmitType }) => {
  const isDialogOpen = open;

  const _handleClose = (e: any) => {
    onClose()
  }

  return (
    <AnimatePresence>
      {isDialogOpen && (
        <motion.div
          className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <ClickAwayListener onClickAway={_handleClose}>
            <div>
              <div className="bg-white rounded-lg shadow-lg w-96">
                {
                  title &&
                  <div className='px-5 pt-5 font-semibold text-lg'>
                    {title}
                  </div>
                }
                <div className='p-5 pt-4'>{children}</div>
                <div className='p-4 text-right bg-gray-50 rounded-b-lg space-x-3'>
                  <button type="button" className='btn btn-default bg-white px-3 rounded-lg' onClick={_handleClose}>Cancel</button>
                  <button type="button" className={`btn ${btnSubmitType ? 'btn-'+btnSubmitType: 'btn-primary'} px-3 rounded-lg`} onClick={() => onSubmit()}>{btnSubmitText ?? 'Submit'}</button>
                </div>
              </div>
            </div>
          </ClickAwayListener>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default DialogConfirmation;
