import { FC, ReactNode, useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { ClickAwayListener } from '@mui/material';

interface ConfirmBoxProps {
  children: ReactNode;
  closable?: Boolean;
  open: Boolean;
  title?: string;
  onClose?: Function;
}

const Dialog: FC<ConfirmBoxProps> = ({ children, open, title }) => {
  const [isDialogOpen,setOpen] = useState(open);

  const _handleClose = (e: any) => {
    setOpen(false)
    console.log(isDialogOpen)
  }

  return (
    <AnimatePresence>
      {isDialogOpen && (
        <motion.div
          className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <ClickAwayListener onClickAway={_handleClose}>
            <div>
              <div className="bg-white rounded shadow-lg w-96">
                {
                  title &&
                  <div className='px-4 pt-4 font-semibold text-lg'>
                    {title}
                  </div>
                }
                <div className='p-4'>{children}</div>
                <div className='p-4 text-right bg-gray-50 rounded-b space-x-3'>
                  <button type="button" className='btn btn-default bg-white px-3'>Cancel</button>
                  <button type="button" className='btn btn-primary px-3'>Submit</button>
                </div>
              </div>
            </div>
          </ClickAwayListener>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default Dialog;
