import React, { ReactNode, useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';

interface CollapsibleProps {
  icon?: ReactNode;
  title: string;
  children?: string | ReactNode;
}

const Collapsible: React.FC<CollapsibleProps> = ({ icon, title, children }) => {
  const [isOpen, setIsOpen] = useState(true);

  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="bg-white rounded-lg my-4">
      <div className="flex items-center justify-between">
        <div className="flex-shrink-0 mr-8 flex items-center space-x-2">
          <div className='text-gray-500'>{icon}</div>
          <div className='font-black text-lg text-gray-500'>{title}</div>
        </div>
        <div className='flex-grow h-[3px] border-2 bg-gray-300'></div>
        <button
          className="text-gray-500 hover:text-gray-700 focus:outline-none flex-shrink-0 ml-4"
          onClick={toggleCollapse}
        >
          <motion.svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
            initial={false}
            animate={isOpen ? "open" : "closed"}
            variants={{
              open: { rotate: 180 },
              closed: { rotate: 0 }
            }}
          >
            <path
              fillRule="evenodd"
              d="M6.293 9.293a1 1 0 011.414 0L10 11.586l2.293-2.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </motion.svg>
        </button>
      </div>
      <AnimatePresence>
        {isOpen && (
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: 'auto' }}
            exit={{ opacity: 0, height: 0 }}
            className="mt-2 p-4"
          >
            {children}
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
};

export default Collapsible;
