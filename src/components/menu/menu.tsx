import { ClickAwayListener } from '@mui/material';
import React, { FC, useState } from 'react';

interface DropdownProps {
  trigger: React.ReactNode;
  children: React.ReactNode;
  origin?: string; 
  className?: string;
}

const Menu: FC<DropdownProps> = ({ trigger, children, origin, className }) => {
  const [isOpen, setIsOpen] = useState<Boolean>(false);
  let originClasses = 'origin-left-center bottom-[80%] right-0'
  if (origin == 'top-right') {
    originClasses = 'origin-left-center bottom-[80%] right-0'
  } else if(origin == 'bottom-right') {
    originClasses = 'origin-left-center top-[100%] right-0'
  }

  const toggleDropdown = (args: Boolean) => {
    setIsOpen(args)
  };

  return (
      <div className="relative text-left">
        <ClickAwayListener onClickAway={() => toggleDropdown(false)}>
          <div>
            <div>
              <button
                type="button"
                className='z-10'
                onClick={() => toggleDropdown(!isOpen)}
              >
                {trigger}
              </button>
            </div>

            {isOpen && (
              <div className={`flex justify-center items-center absolute ${originClasses} mb-2 p-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 z-30 ${className}`}>
                <div className="w-full" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  {children}
                </div>
              </div>
            )}
          </div>
        </ClickAwayListener>
      </div>
  );
};

export default Menu;
