import { isNotification } from "@/src/app/services/notification/notification-type";

export type TodoType = {
  loading?: Boolean;
  onClick?: Function;
  title?: string | ReactNode;
  description?: string | ReactNode;
  priority?: string | ReactNode;
  action?: ReactNode;
  type?: null | 'BASIC' | 'INPUT_LINK_RECORD' | 'COPY_LINK' | 'REVIEW_STUDENT';
  urlToCopy?: string | null;
  label?: string | null;
  data?: isNotification;
}

interface TodoReviewStudentBodyProps {
  registrant: Record<any, any>
}