import { FC } from "react";
import Skeleton from "../skeleton/skeleton";

type ButtonCheckType = {
  loading?: Boolean;
  onClick?: Function;
  clickable?: Boolean;
}

const ButtonCheck: FC<ButtonCheckType> = ({loading,clickable = true,onClick}) => {
  const _handleClick = () => {
    if (clickable) {
      if (onClick) {
        onClick()
      }
    }
  }

  return (
    loading ? 
    <Skeleton className="w-5 h-5 mt-2 rounded-full" />:
    <button type="button" className={`btn rounded-full flex-shrink-0 mt-2 ${clickable ? 'btn-primary': 'bg-gray-200 text-gray-500 cursor-default'}`} onClick={_handleClick}>
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={2.5} stroke="currentColor" className="w-5 h-5">
        <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12.75l6 6 9-13.5" />
      </svg>
    </button>
  )
}

export default ButtonCheck