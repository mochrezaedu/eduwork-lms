import { ReactNode } from "react";

export default function TodoGroup({children,title}: {children: ReactNode | string,title: string}) {
  return (
    <div className="py-2 px-2 bg-gray-200 rounded-lg mx-auto w-[750px]">
      <div className="py-2">{title}</div>
      <div>
        {children}
      </div>
    </div>
  )
}