import moment from "moment";
import Collapsible from "../collapse/collapse";
import { TodoReviewStudentBodyProps } from "./todo-type";
import { useEffect, useState } from "react";
import { AppDispatch } from "@/src/app/types/redux";
import { useDispatch } from "react-redux";
import { eventTodoActions } from "@/src/app/services/event/event-todo-slice";

export default function TodoReviewStudentBodyTodo({registrant}: TodoReviewStudentBodyProps) {
  const dispatch: AppDispatch = useDispatch()
  const [allCheckboxesChecked, setAllCheckboxesChecked] = useState(false);
  const [lateProgressChecked,setLateProgressChecked] = useState((!registrant.last_progress_at ? true: false))
  const [deadlineChecked,setDeadlineChecked] = useState(!registrant.deadline_at ? true: false)
  const [targetCurriculumChecked,setTargetCurriculumChecked] = useState(!registrant.target_late_curriculum ? true: false)

  // Calculate days difference and format as a string
  const calculateDaysDiff = (date: string) => {
    const daysDiff = moment().diff(moment(date), 'days');
    return `${daysDiff} Hari`;
  };

  // Determine if all checkboxes are checked
  // const areAllCheckboxesChecked = () => {
  //   // Implement your logic to determine if all checkboxes are checked
  //   // Update the state variable `allCheckboxesChecked` accordingly
  //   // Example: setAllCheckboxesChecked(true);
  //   setAllCheckboxesChecked(true);
  // };

  useEffect(() => {
    if (lateProgressChecked && deadlineChecked && targetCurriculumChecked) {
      setAllCheckboxesChecked(true);
    } else {
      setAllCheckboxesChecked(false);
    }
  }, [lateProgressChecked, deadlineChecked, targetCurriculumChecked]);

  useEffect(() => {
    if (allCheckboxesChecked) {
      dispatch(eventTodoActions.addReviewedRegistrantIds(registrant.id))
    } else {
      dispatch(eventTodoActions.removeReviewedRegistrantIds(registrant.id))
    }
  }, [allCheckboxesChecked]);

  return (
    <Collapsible icon={
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8">
        <path d="M19.5 21a3 3 0 003-3v-4.5a3 3 0 00-3-3h-15a3 3 0 00-3 3V18a3 3 0 003 3h15zM1.5 10.146V6a3 3 0 013-3h5.379a2.25 2.25 0 011.59.659l2.122 2.121c.14.141.331.22.53.22H19.5a3 3 0 013 3v1.146A4.483 4.483 0 0019.5 9h-15a4.483 4.483 0 00-3 1.146z" />
      </svg>  
    } title="Todo List">
      <div className="space-y-2">
        { 
          Boolean(registrant.last_progress_at && moment().diff(moment(registrant.last_progress_at), 'days') > 7) &&
          <div className="flex items-center space-x-2 rounded hover:active-todo-event">
            <input
              type="checkbox"
              className="w-4 h-4"
              defaultChecked={lateProgressChecked}
              onChange={() => setLateProgressChecked(!lateProgressChecked)}
            />
            <span>Ingatkan siswa tidak ada progress {'>'} <strong className="font-bold">1 minggu</strong> </span>
          </div>
        }
        {Boolean(registrant.deadline_at && moment().diff(moment(registrant.deadline_at), 'days') > 1) &&
          <div className="flex items-center space-x-2 rounded hover:active-todo-event">
            <input
              type="checkbox"
              className="w-4 h-4"
              defaultChecked={deadlineChecked}
              onChange={() => setDeadlineChecked(!deadlineChecked)}
            />
            <span>
              Ingatkan siswa ada deadline terlewat  
              <strong className="ml-1">
                {calculateDaysDiff(registrant.deadline_at)}
              </strong>
            </span>
          </div>
        }
        {Boolean(registrant.target_late_curriculum) &&
          <div className="flex items-center space-x-2 rounded hover:active-todo-event">
            <input
              type="checkbox"
              className="w-4 h-4"
              defaultChecked={targetCurriculumChecked}
              onChange={() => setTargetCurriculumChecked(!targetCurriculumChecked)}
            />
            <span>Siswa sudah memulai kelas {registrant.late_curriculum_diff}, Ingatkan untuk segera mencapai {registrant.late_curriculum.category.title}: {registrant.late_curriculum.title}</span>
          </div>
        }
      </div>
    </Collapsible>
  );
}
