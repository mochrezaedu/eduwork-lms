import React, { useEffect, useState } from "react"
import Collapsible from "../collapse/collapse"
import ReactLoading from "react-loading"
import Alert from "../alert/alert"
import { AppDispatch } from "@/src/app/types/redux";
import { useDispatch } from "react-redux";
import { eventTodoActions } from "@/src/app/services/event/event-todo-slice";
import { storeFeedbackMentor } from "@/src/app/services/event/api";
import { useParams } from "react-router-dom";

interface TodoReviewStudentBodyReviewFormProps {
  registrant: Record<any,any>;
}

export default function TodoReviewStudentBodyReviewForm({registrant}: TodoReviewStudentBodyReviewFormProps) {
  const params: Record<any, any> = useParams()
  const [loading, setLoading] = useState(false)
  const dispatch: AppDispatch = useDispatch()
  const [status, setStatus] = useState(registrant.student?.feedback_from_mentor?.status ?? '')
  const [problem, setProblem] = useState<string>(registrant.student?.feedback_from_mentor?.problems[0]?.type)
  const [action,setAction] = useState<string>(registrant.student?.feedback_from_mentor?.problem_action ?? '')
  const [canSubmit, setCanSubmit] = useState(false)
  const mentorFeedback = registrant.feedback_from_mentor
  
  const _handleSubmit = () => {
    if (!loading && canSubmit) {
      setLoading(true)
      dispatch(storeFeedbackMentor({
        payload: {
          registrant_id: registrant.id,
          status,problem,action,
          problem_types: JSON.stringify([problem]),
          event_id: params.id
        } 
      }))
      dispatch(eventTodoActions.storeMentorFeedback(registrant.id))
    }
  }

  const _handleChangeStatus = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value === 'lancar') {
      setStatus('lancar')      
    } else if(e.target.value === 'bermasalah') {
      setStatus('bermasalah')
    } else {
      setStatus(null);      
    }
  }

  const _handleChangeProblem = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setProblem(e.target.value)
  }

  const _handleChangeProblemAction = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setAction(e.target.value)
  }

  useEffect(() => {
    if(Boolean(status === 'lancar') || (Boolean(status === 'bermasalah' && problem && action))) {
      setCanSubmit(true)
    }
  }, [status,problem,action])

  useEffect(() => {
    if (mentorFeedback) {
      setLoading(false)
    }
  }, [mentorFeedback])

  return (
    <Collapsible icon={
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8">
        <path d="M19.5 21a3 3 0 003-3v-4.5a3 3 0 00-3-3h-15a3 3 0 00-3 3V18a3 3 0 003 3h15zM1.5 10.146V6a3 3 0 013-3h5.379a2.25 2.25 0 011.59.659l2.122 2.121c.14.141.331.22.53.22H19.5a3 3 0 013 3v1.146A4.483 4.483 0 0019.5 9h-15a4.483 4.483 0 00-3 1.146z" />
      </svg>        
    } title="Report Progress Siswa Minggu Ini">
      {
        Boolean(mentorFeedback) &&
        <div className="mb-5">
          <Alert type="success" title={'Anda sudah mereview siswa ini.'} />
        </div>
      }
      {
        !loading &&
        <div className="space-y-3">
          <div className="flex items-center space-x-4">
            <div className="w-[30%] flex-shrink-0">
              Progress 1 bulan terakhir ?
            </div>
            <div className="flex-grow flex items-center space-x-4">
              <div>
                <input type="radio" name={`progs${registrant.id}`} id="" value={'lancar'} onChange={_handleChangeStatus} defaultChecked={Boolean(registrant.student.feedback_from_mentor?.status == 'lancar')} />
                <span className="ml-2">Lancar</span>
              </div>
              <div>
                <input type="radio" name={`progs${registrant.id}`} id="" value={'bermasalah'} onChange={_handleChangeStatus} defaultChecked={Boolean(registrant.student.feedback_from_mentor?.status == 'bermasalah')} />
                <span className="ml-2">Bermasalah</span>
              </div>
            </div>
          </div>
          {
            Boolean(status === 'bermasalah') &&
            <>
              <div className="flex items-center">
                <div className="w-[30%] flex-shrink-0">
                  Problem
                </div>
                <select name="" id="" className="bg-white w-full py-2 px-4 rounded-lg border" onChange={_handleChangeProblem} defaultValue={problem}>
                  <option value="">Choose Problem</option>
                  <option value="progress">Progress</option>
                  <option value="komunikasi">Komunikasi</option>
                  <option value="skill">Skill</option>
                </select>
              </div>
              <div className="flex items-center">
                <div className="w-[30%] flex-shrink-0">
                  Problem Action
                </div>
                <select name="" id="" className="bg-white w-full py-2 px-4 rounded-lg border" onChange={_handleChangeProblemAction} defaultValue={action}>
                  <option value="">Choose Action</option>
                  <option value="gpp ditolerir dl, coba liat perkembangan 1 minggu lagi">gpp ditolerir dl, coba liat perkembangan 1 minggu lagi</option>
                  <option value="saya hubungi sendiri dulu">saya hubungi sendiri dulu</option>
                  <option value="minta tolong eduwork hubungi siswa">minta tolong eduwork hubungi siswa</option>
                  <option value="Siswa tidak aktif dan tidak kooperatif , DO saja">Siswa tidak aktif dan tidak kooperatif , DO saja</option>
                </select>
              </div>
            </>
          }
        </div>
      }
      {
        loading &&
        <div className="flex items-center justify-center">
          <ReactLoading type={'bubbles'} color={'#CACACA'} height={100} width={100} />
        </div>
      }
      <button type="button" className={`w-full py-2 rounded-lg text-white mt-6 ${loading ? 'bg-gray-300 cursor-default': (canSubmit ? 'bg-primary':'bg-gray-300 cursor-default')}`} onClick={_handleSubmit}>
        {
          Boolean(registrant.student.feedback_from_mentor) ? 'Update': 'Submit'
        }
      </button>
    </Collapsible>
  )
}