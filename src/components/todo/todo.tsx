import { FC, useState } from "react";
import Skeleton from "../skeleton/skeleton"
import ButtonCheck from "./button-check"
import TodoWrapper from "./wrapper"
import { TodoType } from "./todo-type";

const Todo: FC<TodoType> = ({title,description,loading,priority,action,onClick = () => {}}) => {
  const [openBody, setOpenBody] = useState(true)
  return (
    <TodoWrapper
    className="w-full text-xs" 
    body={
      loading ? 
      <Skeleton className="h-8 rounded w-full" />:
      description
    } 
    togglePanel={() => setOpenBody(!openBody)}
    openPanel={openBody}
    action={action ?? <ButtonCheck loading={loading} onClick={onClick} />} 
    title={
      <>
        {
          loading ? 
          <Skeleton className="h-8 rounded w-[100px]" />:
          priority &&
          <span className="relative bg-red-100 px-3 py-1 rounded-lg text-xs flex items-center space-x-2">
            <div className="w-3 h-3 rounded-full bg-red-600"></div>
            <span className="text-xs">{priority}</span>
          </span>
        }
        {
          loading ?
          <Skeleton />:
          <span>{title}</span>
        } 
      </>
    } />
  )
}

export default Todo