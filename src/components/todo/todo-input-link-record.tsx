import React, { FC, useEffect, useState } from "react"
import Skeleton from "../skeleton/skeleton"
import { TodoType } from "./todo-type"
import TodoWrapper from "./wrapper"
import { AppDispatch } from "@/src/app/types/redux"
import { useDispatch } from "react-redux"
import { notificationActions } from "@/src/app/services/notification/notification-slice"
import ButtonCheck from "./button-check"

const TodoInputLinkRecord: FC<TodoType> = ({title,description,loading,priority,action,type,label,urlToCopy,onClick = () => {}, data}) => {
  const [openBody, setOpenBody] = useState(true)
  const [input, setInput] = useState('')
  const dispatch: AppDispatch = useDispatch()
  let debounceInput: null | ReturnType<typeof setTimeout> = null

  const _handleCheck = (id: number) => {
    dispatch(notificationActions.readNotification(id))
  }

  const _handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (debounceInput != null) {
      clearTimeout(debounceInput)
      debounceInput = null
    }
    debounceInput = setTimeout(() => {
      setInput(e.target.value)
    }, 1000)
  }

  useEffect(() => {
    if (input) {
      dispatch(notificationActions.readableNotification(data?.id))
    } else {
      dispatch(notificationActions.unreadableNotification(data?.id))
    }
  }, [input])

  return (
    <TodoWrapper
    className="w-full text xs"
    body={
      loading ? 
      <Skeleton className="h-8 rounded w-full" />:
      <div className="w-full">
        <div className="flex-grow">{description}</div>
        <div className="flex items-center w-full space-x-3">
          <div className="flex-shrink-0 text-xs">{label}</div>
          <div className="flex-grow">
            <input type="text" className="w-full rounded-lg border p-2" id="" onChange={_handleInputChange} />
          </div>
          <button type="button" className="btn btn-primary flex-shrink-0 text-xs">Submit</button>
        </div>
      </div>
    } 
    togglePanel={() => setOpenBody(!openBody)}
    openPanel={openBody}
    action={<ButtonCheck clickable={data?.can_read} onClick={() => {_handleCheck(data?.id ?? -1)}} />} 
    title={
      <>
        {
          loading ? 
          <Skeleton className="h-8 rounded w-[100px]" />:
          priority &&
          <span className="relative bg-red-100 px-3 py-1 rounded-lg text-sm flex items-center space-x-2 text-xs">
            <div className="w-2 h-2 rounded-full bg-red-600"></div>
            <span className="text-xs">{priority}</span>
          </span>
        }
        {
          loading ?
          <Skeleton />:
          <span className="text-xs">{title}</span>
        } 
      </>
    } />
  )
}

export default TodoInputLinkRecord