import { config } from "@/src/app/config"
import TodoWrapper from "./wrapper"
import Skeleton from "../skeleton/skeleton"
import { isNotification } from "@/src/app/services/notification/notification-type";
import { useDispatch, useSelector } from "react-redux";
import { eventTodoActions, eventTodoReviewedRegistrantById, getEventTodoLoads } from "@/src/app/services/event/event-todo-slice";
import { AppDispatch, RootState } from "@/src/app/types/redux";
import { useEffect, useState } from "react";
import TodoReviewStudentBody from "./todo-review-student-body";
import ButtonCheck from "./button-check";
import { notificationActions } from "@/src/app/services/notification/notification-slice";

interface TodoReviewStudentItemProps {
  item: Record<any,any>;
  data: isNotification;
}

export default function TodoReviewStudentItem({item,data}: TodoReviewStudentItemProps) {
  const {title,description,priority} = data
  const dispatch: AppDispatch = useDispatch()
  const loading = useSelector(getEventTodoLoads).includes("fetch_registrants_for_mentor_reviews")
  const reviewedRegistrant = useSelector((state: RootState) => eventTodoReviewedRegistrantById(state, item.id))
  const [openBody, setOpenBody] = useState(true)
  
  let photoUrl = config.APP_URL
  if (item.student?.photo) {
    photoUrl = photoUrl + "/storage/user/" + item.student?.id + "/" + item.student?.photo
  } else {
    photoUrl = photoUrl + "/images/avatar/noimage.jpg"
  }

  const _handleCheck = (id: number) => {
    dispatch(eventTodoActions.readNotificationIds(item.id))
  }

  useEffect(() => {
    if (Boolean(reviewedRegistrant)) {
      dispatch(notificationActions.readableNotification(data.id))
    } else {
      dispatch(notificationActions.unreadableNotification(data.id))
    }
  }, [reviewedRegistrant])

  return (
    <TodoWrapper
    className="w-full"
    body={
      loading ? 
      <Skeleton className="h-8 rounded w-full" />:
      <div className="w-full">
        <div className="flex-grow">{description}</div>
        <div className="w-full space-y-3 pb-4">
          
          <div className="flex items-center space-x-6">
            <img src={photoUrl} alt="" className="w-[64px] h-[64px] rounded-full flex-shrink-0" />
            <div className="space-y-2 w-[30%]">
              <div className="">{item.student?.name}</div>
              <div className="flex items-center space-x-3">
                <div className="text-primary p-2 rounded-lg bg-blue-200">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-4 h-4">
                    <path fillRule="evenodd" d="M14.615 1.595a.75.75 0 01.359.852L12.982 9.75h7.268a.75.75 0 01.548 1.262l-10.5 11.25a.75.75 0 01-1.272-.71l1.992-7.302H3.75a.75.75 0 01-.548-1.262l10.5-11.25a.75.75 0 01.913-.143z" clipRule="evenodd" />
                  </svg>
                </div>
                <div className="flex flex-col text-xs text-gray-800">
                  <span className="">Last Active</span>
                  <span>20 Menit Lalu</span>
                </div>
              </div>
            </div>
            <div className="flex-grow">
              <div className="text-gray-600">Learning Progress</div>
              <div className="mb-1 text-gray-600">{item.student?.progress}%</div>
              <div className="w-full h-2 bg-gray-200 rounded-full">
                <div className="h-full bg-gray-100 rounded-full" style={{ width: `100%` }}></div>
                <div className="h-full bg-red-500 rounded-full -mt-2" style={{ width: `${item.student?.progress}%` }}></div>
                <div className="h-full bg-blue-500 rounded-full -mt-4" style={{ width: `${item.student?.mentor_progress ?? 0}%` }}></div>
              </div>
            </div>
            <div className="flex-shrink-0">
              <a href={`https://wa.me/send?phone=${item.student.phone}`} target="_blank" className="btn btn-default text-green-500 border border-gray-300">
                <svg xmlns="http://www.w3.org/2000/svg" 
                fill="currentColor" 
                className="w-4 h-4" viewBox="0 0 16 16"> 
                  <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/> 
                </svg>
              </a>
            </div>
          </div>
        </div>
        <TodoReviewStudentBody registrant={item} />
      </div>
    } 
    togglePanel={() => setOpenBody(!openBody)}
    openPanel={openBody}
    action={<ButtonCheck clickable={Boolean(reviewedRegistrant && item.feedback_from_mentor)} loading={loading} onClick={() => {_handleCheck(item.id)}} />} 
    title={
      <>
        {
          loading ? 
          <Skeleton className="h-8 rounded w-[100px]" />:
          priority &&
          <span className="relative bg-red-100 px-4 py-1 rounded-lg text-sm flex items-center space-x-2">
            <div className="w-2 h-2 rounded-full bg-red-600"></div>
            <span className="text-xs">{priority}</span>
          </span>
        }
        {
          loading ?
          <Skeleton />:
          <span>{title}</span>
        } 
      </>
    } />
  )
}