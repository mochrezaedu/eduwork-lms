import TodoReviewStudentBodyInformation from "./todo-review-student-body-information";
import TodoReviewStudentBodyLVReport from "./todo-review-student-body-lv-report";
import TodoReviewStudentBodyReviewForm from "./todo-review-student-body-review-form";
import TodoReviewStudentBodyTodo from "./todo-review-student-body-todo";
import { TodoReviewStudentBodyProps } from "./todo-type";

export default function TodoReviewStudentBody({registrant}: TodoReviewStudentBodyProps) {
  return (
    <div className="">
      <TodoReviewStudentBodyInformation registrant={registrant} />
      <TodoReviewStudentBodyTodo registrant={registrant} />
      <TodoReviewStudentBodyLVReport />
      <TodoReviewStudentBodyReviewForm registrant={registrant} />
    </div>
  )
}