import { isNotification } from "@/src/app/services/notification/notification-type"
import { useSelector } from "react-redux"
import TodoReviewStudentItem from "./todo-review-student-item"
import { registrantEventMentorReviewsNotRead } from "@/src/app/services/event/event-todo-slice"

const TodoReviewStudent = ({data}: {data: isNotification}) => {
  const registrants = useSelector(registrantEventMentorReviewsNotRead)
  return (
    <div className="space-y-4 text-xs"> 
      {
        registrants?.map((item) => {
          return <TodoReviewStudentItem 
          key={item.id}
          item={item} data={data} />  
        })
      }
    </div>
  )
}

export default TodoReviewStudent