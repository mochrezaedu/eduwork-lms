import { AnimatePresence,motion } from "framer-motion";
import { ReactNode } from "react"

interface TodoWrapper {
  action: ReactNode;
  title: ReactNode;
  body?: string | ReactNode;
  togglePanel: Function;
  openPanel: Boolean;
  className?: string;
}

export default function TodoWrapper({action,title,body,openPanel,togglePanel, className}: TodoWrapper) {
  return (
    <div className={`flex items-start w-[750px] mx-auto rounded-lg space-x-3 ${className}`}>
      <div className="rounded-lg p-1 bg-white flex-grow border border-blue-200 hover:shadow-lg">
        <div className="rounded-lg flex-grow">
          <div className={`bg-[#EDF3FF] py-3 px-4 space-x-2 flex items-center justify-between w-full ${Boolean(openPanel && body) ? 'rounded-t-lg': 'rounded-lg'}`}>
            <div className="flex items-center space-x-2">
              {
                title
              }
            </div>
            {
              body &&
              <AnimatePresence>
                <motion.div
                  animate={{ rotate: Boolean(openPanel && body) ? 180: 0 }}
                  transition={{ duration: 0.1 }}
                >
                  <button type="button" className="text-blue-500" onClick={() => {togglePanel()}}>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                    </svg>
                  </button>
                </motion.div>
              </AnimatePresence>
            }
          </div>
          <AnimatePresence initial={false}>
            {Boolean(openPanel && body) && (
              <motion.div
                initial={{ maxHeight: '0%', opacity: 1 }}
                animate={{ maxHeight: '100%', opacity: 1 }}
                exit={{ height: 0, opacity: 1 }}
                transition={{ duration: 0.5 }}
                className="overflow-hidden rounded-b-lg"
              >
                <div className="bg-white rounded-b-lg py-5 px-4 z-[99] z-[97]">
                  {
                    body
                  }
                </div>
              </motion.div>
            )}  
          </AnimatePresence>
        </div>
      </div>
      {
        action
      }
    </div>
  )
}