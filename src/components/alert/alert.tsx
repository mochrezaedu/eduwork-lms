import { AlertProps } from "./alert-type";

export default function Alert({title,type,children = ''}: AlertProps) {
  let _bg = 'bg-gray-100'
  let _cr = 'text-gray-600 border-gray-700'

  if (type === 'danger') {
    _bg = 'bg-red-100'
    _cr = 'text-red-600 border-red-700'
  }
  if (type === 'success') {
    _bg = 'bg-green-100'
    _cr = 'text-green-600 border-green-700'
  }
  return (
    <div className={`p-4 rounded-lg border-l-4 ${children ? 'space-y-3': ''} ${_bg} ${_cr}`}>
      <div className="flex space-x-3 items-center">
        {
          type == 'danger' && 
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
            <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z" />
          </svg>
        }
        {
          type == 'success' &&
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75L11.25 15 15 9.75m-3-7.036A11.959 11.959 0 013.598 6 11.99 11.99 0 003 9.749c0 5.592 3.824 10.29 9 11.623 5.176-1.332 9-6.03 9-11.622 0-1.31-.21-2.571-.598-3.751h-.152c-3.196 0-6.1-1.248-8.25-3.285z" />
          </svg>

        }
        <div className="font-semibold">{title}</div>
      </div>
      <div>{children}</div>
    </div>
  )
}