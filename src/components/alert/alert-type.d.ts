import {ReactNode} from 'react'


export interface AlertProps {
  title: string | ReactNode;
  children?: string | ReactNode;
  type: string;
}