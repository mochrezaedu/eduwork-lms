import { InputProps } from "./form";


export default function Input(props: InputProps) {
  const {label,className,...rest} = props
  return (
    <div className={`${className}`}>
      <label className="block text-gray-700 text-sm font-bold mb-2">
        {label}
      </label>
      <input className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" {...rest} />
    </div>
  )
}