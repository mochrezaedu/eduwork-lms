export interface InputProps extends Omit<HTMLProps<HTMLInputElement>, 'label'> {
  label?: string;
}