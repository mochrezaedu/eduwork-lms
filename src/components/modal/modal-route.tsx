import { FC } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
// import { useDispatch, useSelector } from 'react-redux';
// import { RootState } from '@/src/app/ReduxStore';
// import { closeMyModal } from './slice-modal'; // Import your modal slice action
import { ClickAwayListener } from '@mui/base/ClickAwayListener';
import { useHistory } from 'react-router-dom';
import { ModalProps } from './modal-type';


const ModalRoute: FC<ModalProps> = ({ children,title,className,fullscreen }) => {
  const web = useHistory()
  const handleClose = () => {
    web.goBack();
  }

  return (
    <AnimatePresence>
      <motion.div
        className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        <ClickAwayListener onClickAway={handleClose}>
          <div className={`bg-white px-4 py-3 rounded shadow-lg  ${fullscreen ? 'w-full h-full':'max-w-[500px]'} ${className}`}>
            <div className={'flex items-center justify-between'}>
              <div>
                {title || ''}
              </div>
              <button
                className="bg-gray-100 rounded-full text-gray-500 hover:text-gray-800 hover:bg-gray-200 p-2"
                onClick={handleClose}
              >
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                </svg>
              </button>
            </div>
            {children}
          </div>
        </ClickAwayListener>
      </motion.div>
    </AnimatePresence>
  );
};

export default ModalRoute;
