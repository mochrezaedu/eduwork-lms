import { ModalState } from '@/src/app/types/modal';
import { RootState } from '@/src/app/types/redux';
import { createSlice } from '@reduxjs/toolkit';

const initialState: ModalState = {
  name: '',
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openMyModal: (state, payload) => {
      state.name = payload.payload;
    },
    closeMyModal: (state) => {
      state.name = '';
    },
  },
});

export const { openMyModal, closeMyModal } = modalSlice.actions;

export const modalStates = (state: RootState) => state.modal

export default modalSlice;
