import { FC } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { ClickAwayListener } from '@mui/material';
import {ModalProps} from './modal-type'
import { useHistory } from 'react-router-dom';


const Modal: FC<ModalProps> = ({ children,type,closable = true,open,onClose = () => {}, title,className,fullscreen}) => {
  const isModalOpen = (open === undefined ? true: (open === false ? false: true));
  const web = useHistory()

  const _handleClose = (e: any) => {
    if(type === 'route') {
      web.goBack();
    } else {
      if (closable) {
        onClose(e)
      }
    }
  }

  return (
    <AnimatePresence>
      {isModalOpen && (
        <motion.div
          className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <ClickAwayListener onClickAway={_handleClose}>
            <div className={`bg-white px-4 py-3 rounded shadow-lg ${fullscreen ? 'w-full h-full':'max-w-[500px]'} ${className}`}>
              {
                closable &&
                <div className={'flex items-center justify-between'}>
                  <div>
                    {title || ''}
                  </div>
                  <button
                    className="bg-gray-100 rounded-full text-gray-500 hover:text-gray-800 hover:bg-gray-200 p-2"
                    onClick={_handleClose}
                  >
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                  </button>
                </div>
              }
              <div className={`w-full small-scroll ${fullscreen ? '': 'max-h-[80vh] overflow-hidden hover:overflow-y-auto'}`}>
                {children}
              </div>
            </div>
          </ClickAwayListener>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default Modal;
