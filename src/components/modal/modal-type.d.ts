import { ReactNode } from 'react'


export type ModalProps = {
  children: ReactNode;
  closable?: Boolean;
  open?: Boolean;
  onClose?: Function;
  title?: string | ReactNode;
  className?: string;
  fullscreen?: Boolean;
  type?: string;
}