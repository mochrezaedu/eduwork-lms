import ReactDOM from 'react-dom/client'
import './index.css'
import { Provider } from 'react-redux'
import ReduxStore from './app/ReduxStore.js'
import {
  BrowserRouter
} from "react-router-dom";
import Layout from './pages/layout.jsx'

const rootElement: HTMLElement | null = document.getElementById('root') as HTMLElement
ReactDOM.createRoot(rootElement).render(
  <Provider store={ReduxStore}>
    <BrowserRouter>
      <Layout />
    </BrowserRouter>
  </Provider>,
)
