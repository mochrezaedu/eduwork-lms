import {Switch,Route} from 'react-router-dom'

import Sidebar from '../pages/sidebar';

import Notification from '../pages/notification/notification';
import Event from '../pages/event/event';
import Navbar from '../pages/navbar';
import Student from '../pages/student/student';
import Class from '../pages/class/class';
import StudentNoteModal from '../pages/student/student-note-modal';
import { useSelector } from 'react-redux';
import Splash from '../pages/init/splash';
import { getLoads } from '../pages/init/init-slice';
// import { config as AppConfig } from '../app/config';
import PostLoading from '../pages/error/post-splash';
import { getAuth } from '../app/services/auth/auth-slice';
import { useEffect, useState } from 'react';
import EventProgress from '../pages/event/event-todo';
import EventUpdate from '../pages/event/event-update';
import Modal from '../components/modal/modal';

function Router(children: { children?: String | null | undefined }) {
  const initLoads = useSelector(getLoads)
  const initLoadCount = initLoads.length
  const auth = useSelector(getAuth)
  const user = auth.user
  const [states, setStates] = useState('pending')

  useEffect(() => {
    if(initLoadCount < 1) {
      setTimeout(() => {
        if (!user) {
          setStates('unauthorized')
        } else {
          setStates('authorized')
        }
      }, 2000);
    } else {
      setStates('pending')
    }
  }, [user,initLoadCount])

  return (
    <>
      {
        states == 'pending' && 
        <Splash />
      }
      {
        states == 'unauthorized' &&
        <PostLoading message={!user ? "": ""} />
      }
      {
        states == 'authorized' &&
        <>
          <Switch>
            <>
              <div className="flex items-start bg-blue-50">
                <Sidebar />
                <div className='min-h-[100vh] bg-blue-50 flex-grow'>
                  <Navbar />
                  <div className='p-4 mt-[60px]'>
                    <div className='w-full p-2 space-y-6'>
                      <Route path="/" component={Notification} exact />
                      <Route path="/notification" component={Notification} />
                      <Route path="/event" render={({ match: { url } }) => (
                        <>
                          <Route path="/event/:id/progress" component={EventProgress} exact />
                          <Route path={`/event/:id/update`} exact>
                            <Modal title={'Event Update'} type={'route'}>
                              <EventUpdate />
                            </Modal>
                          </Route>
                          <Route path={`${url}/`} component={Event} />
                        </>
                      )} />
                      <Route path="/student" component={Student} />
                      <Route path="/class" component={Class} />
                    </div>
                  </div>
                </div>
              </div>
            </>
          </Switch>
          <Switch>
            <Route path={`/student/notes`}>
              <StudentNoteModal />
            </Route>
            
          </Switch>
        </>
      }
    </>
  )
}

export default Router

